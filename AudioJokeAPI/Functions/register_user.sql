CREATE OR REPLACE FUNCTION register_user(_token varchar, _nickname varchar) RETURNS Users AS
$$
DECLARE
    session_                   sessions%ROWTYPE := check_session(_token);
    user_                      users%ROWTYPE;
BEGIN
    SELECT * INTO user_ FROM users WHERE id = session_.user_id LIMIT 1;
    IF user_.is_registered
    THEN
        RAISE EXCEPTION 'REGISTER_USER_001' USING HINT = 'User is already registered';
    END IF;

    user_.nickname = _nickname;
    user_.is_registered = true;

    UPDATE users
    SET (nickname, is_registered) = (user_.nickname, user_.is_registered)
    WHERE id = user_.id;

    RETURN user_;
END;
$$ LANGUAGE plpgsql;
