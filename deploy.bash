echo "$SSH_PRIVATE_KEY" > api_deploy_private_key;
chmod 700 api_deploy_private_key;
ssh -i ./api_deploy_private_key sqr@norfoe.com;
cd sqr-project/api
git pull
docker-compose down
sudo systemctl restart postgresql
docker-compose build
docker-compose up -d
exit
