﻿using GraphQL;
using AudioJokeAPI.ErrorHandling;
using System;
using AudioJokeAPI.Schema;

namespace AudioJokeAPI.Services
{
    public class SchemaValidation: ISchemaValidation
    {
        private readonly IServiceProvider _provider;

        public SchemaValidation(IServiceProvider provider)
        {
            _provider = provider;
        }

        public bool GetValidatableArgument<TContext, TResult>(IResolveFieldContext<TContext> context, string name, out TResult result) where TResult : IGraphQLValidatable
        {
            return GetValidatedArgument(context, name, out result, x => x.IsValid(_provider));
        }

        public bool GetTokenArgument(IResolveFieldContext<object> context, string name, out string result)
        {
            return GetValidatedArgument(context, name, out result, x => !string.IsNullOrWhiteSpace(x));
        }

        private bool GetValidatedArgument<TContext, TResult>(IResolveFieldContext<TContext> context, string name, out TResult result, Func<TResult, bool> validator)
        {
            result = context.GetArgument<TResult>(name);
            if (!validator(result))
            {
                context.Errors.Add(GraphQLErrorHandler.NewValidationError());
                return false;
            }
            return true;
        }
    }
}
