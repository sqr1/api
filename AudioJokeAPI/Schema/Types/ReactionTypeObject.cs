﻿using GraphQL.Types;
using AudioJokeAPI.Models;

namespace AudioJokeAPI.Schema.Types
{
    public class ReactionTypeObject : EnumerationGraphType<ReactionType>
    {
        public ReactionTypeObject()
        {
            Name = "ReactionType";
        }
    }
}
