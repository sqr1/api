﻿using System;

namespace AudioJokeAPI.Services
{
    public interface IClientService
    {
        public string GetHost();

        public Guid GetUserId();

        public bool IsAdmin();

        public bool IsRegistered();

        public string GetAccessToken();

        public void SetAccessToken(string token);

        public void DeleteAccessToken();
    }
}
