CREATE OR REPLACE FUNCTION check_session(_token varchar) RETURNS sessions
    LANGUAGE plpgsql
AS
$$
DECLARE
    session_ sessions%ROWTYPE;
BEGIN
    SELECT * INTO session_ FROM sessions WHERE access_token = _token AND access_token_expiry_date > now() LIMIT 1;
    IF session_ IS NULL THEN    
        SELECT * FROM RAISE_INVALID_TOKEN_EXCEPTION();
    END IF;
    RETURN session_;
END;
$$;

