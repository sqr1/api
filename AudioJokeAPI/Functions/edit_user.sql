CREATE OR REPLACE FUNCTION edit_user(_token varchar, _id uuid, _nickname varchar, _bio varchar) RETURNS BOOLEAN AS
$$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    user_    Users%ROWTYPE;
BEGIN
    IF session_.user_id <> _id
    THEN
        IF (SELECT is_admin FROM is_admin(session_.user_id) LIMIT 1)
        THEN
            -- user tries to modify another user while having no permission to do that
            RAISE EXCEPTION 'EDIT_USER_001' USING HINT = 'Forbidden';
        END IF;
    END IF;

    SELECT * INTO user_ from Users where id = _id LIMIT 1;
    IF user_ IS NULL THEN
        RAISE EXCEPTION 'EDIT_USER_002' USING HINT = 'Invalid user id';
    END IF;

    IF NOT user_.is_registered
    THEN
        RAISE EXCEPTION 'EDIT_USER_003' USING HINT = 'Unregistered user';
    END IF;

    user_.nickname = COALESCE(_nickname, user_.nickname);
    user_.bio = COALESCE(_bio, user_.bio);

    UPDATE users
    SET (nickname, bio) = (user_.nickname, user_.bio)
    WHERE users.id = user_.id;

    RETURN true;
END;
$$ LANGUAGE plpgsql;
