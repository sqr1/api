CREATE OR REPLACE FUNCTION create_reaction(_token varchar, _joke_id uuid, _type reaction_type) RETURNS reactions AS $$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    joke_ jokes%ROWTYPE;
    reaction_id_ uuid;
    reaction_ Reactions%ROWTYPE;
BEGIN
    IF (SELECT id FROM users WHERE id = session_.user_id AND is_registered LIMIT 1) IS NULL
    THEN
        RAISE EXCEPTION 'CREATE_REACTION_001' USING HINT = 'Unregistered user';
    END IF;

    SELECT * INTO joke_ FROM jokes WHERE id = _joke_id LIMIT 1;
    IF joke_ IS NULL
    THEN
        RAISE EXCEPTION 'CREATE_REACTION_002' USING HINT = 'Invalid joke id';
    END IF;
    
    IF (SELECT id FROM reactions WHERE user_id = session_.user_id AND joke_id = _joke_id) IS NOT NULL
    THEN
        RAISE EXCEPTION 'CREATE_REACTION_003' USING HINT = 'Duplicative reaction';
    END IF;

    reaction_id_ := uuid_generate_v4();
    INSERT INTO reactions(id, user_id, joke_id, type) VALUES (reaction_id_, session_.user_id, _joke_id, _type);
    
    SELECT * INTO reaction_ FROM reactions WHERE id = reaction_id_ LIMIT 1;
    
    RETURN reaction_;
END;
$$ LANGUAGE plpgsql;

