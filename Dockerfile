FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY ./AudioJokeAPI/*.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY ./AudioJokeAPI ./
RUN dotnet publish -c Release -o out

RUN dotnet run migration-down
RUN dotnet run migration-up
RUN dotnet run deploy-functions

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 5000
ENTRYPOINT ["dotnet", "AudioJokeAPI.dll", "--environment=Development"]

