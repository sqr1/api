﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AudioJokeAPITest.Helpers
{
    public static class FileLoader
    {
        public static byte[] LoadFile(string filename)
        {
            return File.ReadAllBytes(
                Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "Files",
                    filename));
        }

        public static string LoadBase64File(string filename)
        {
            return Convert.ToBase64String(LoadFile(filename));
        }
    }
}
