﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AudioJokeAPI.Models
{
    [Table("reactions")]
    public partial class Reaction : BaseModel<Reaction>
    {
        public Guid UserId { get; set; }
        public Guid JokeId { get; set; }
        public ReactionType Type { get; set; }

        public virtual Joke Joke { get; set; }
        public virtual User User { get; set; }
    }
}
