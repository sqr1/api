﻿using GraphQL.Types;
using AudioJokeAPI.Models;
using AudioJokeAPI.Services;

namespace AudioJokeAPI.Schema.Types
{
    public sealed class JokeObject : ObjectGraphType<Joke>
    {
        public JokeObject(ICDNService cdnService)
        {
            Name = "Joke";
            Field("id", x => x.Id);
            Field("text", x => x.Text);
            Field("audioUrl", x => cdnService.GetAudioUrl(x.Id));
        }
    }
}
