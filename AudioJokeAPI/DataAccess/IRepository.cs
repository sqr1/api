﻿using AudioJokeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AudioJokeAPI.DataAccess
{
    public interface IRepository
    {
        public Task<IDictionary<Guid, T>> GetModelsByIdAsync<T>(IEnumerable<Guid> ids, CancellationToken cancellationToken) where T : BaseModel<T>;

        public User GetUserByAccessToken(string accessToken);

        public PagedArray<T> GetPage<T>(int page, int perPage, System.Linq.Expressions.Expression<Func<T, bool>> predicate = null) where T : BaseModel<T>;

        public PagedArray<T> GetPage<T>(int page, int perPage, IQueryable<T> queryable) where T : BaseModel<T>;

        public Joke CreateJoke(string accessToken, string text);

        public Reaction CreateReaction(string accessToken, Guid id, ReactionType type);

        public void DeleteJoke(Guid id);

        public void DeleteJoke(string accessToken, Guid id);

        public void DeleteReaction(string accessToken, Guid id);

        public void DeleteUser(string accessToken, Guid id);

        public void EditUser(string accessToken, Guid id, string nickname, string bio);

        public IQueryable<Joke> GetReactedJokes(Guid userId, ReactionType reactionType);

        public IQueryable<Joke> GetPublishedJokes(Guid userId);

        public void Logout(string accessToken);

        public Session CreateSession(string email, string ip);

        public Session Refresh(string accessToken);

        public User RegisterUser(string accessToken, string nickname);

        public void DeleteExpiredSesssions();

        public void DeleteExpiredUsers();

        public Joke TakeNextRecommendedJoke(string accessToken);
    }
}
