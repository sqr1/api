﻿using GraphQL;
using GraphQL.Types;
using AudioJokeAPI.DataAccess;
using AudioJokeAPI.ErrorHandling;
using AudioJokeAPI.Models;
using AudioJokeAPI.Models.Inputs;
using AudioJokeAPI.Schema.Inputs;
using AudioJokeAPI.Services;

namespace AudioJokeAPI.Schema.Types
{
    public sealed class UserObject : ObjectGraphType<User>
    {
        public UserObject(IClientService clientService,
            IRepository repository,
            ISchemaValidation schemaValidation)
        {
            Name = "User";
            Field("id", x => x.Id);
            Field<StringGraphType>("email", resolve: context =>
            {
                if (clientService.IsAdmin() || context.Source.Id == clientService.GetUserId())
                {
                    return context.Source.Email;
                }
                context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                return null;
            });
            Field("nickname", x => x.Nickname);
            Field("bio", x => x.Bio);

            Field<BooleanGraphType>("isRegistered",
                resolve: context =>
                {
                    return context.Source.IsRegistered;
                });

            Field<PagedArrayObject<JokeObject, Joke>>(
                "reactedJokes",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<NewPagedArrayObject>>
                    {
                        Name = "pagedArray"
                    },
                    new QueryArgument<NonNullGraphType<ReactionTypeObject>>
                    {
                        Name = "reactionType"
                    }),
                resolve: context =>
                {
                    if (clientService.IsAdmin() || clientService.GetUserId() == context.Source.Id)
                    {
                        if (!schemaValidation.GetValidatableArgument(context, "pagedArray", out NewPagedArray pagedArray))
                        {
                            return null;
                        }
                        var reactionType = context.GetArgument<ReactionType>("reactionType");
                        return repository.GetPage(pagedArray.Page, pagedArray.PerPage, repository.GetReactedJokes(context.Source.Id, reactionType));
                    }
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;
                }
            );
            Field<PagedArrayObject<JokeObject, Joke>>(
                "publishedJokes",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<NewPagedArrayObject>>
                    {
                        Name = "pagedArray"
                    }),
                resolve: context =>
                {
                    if (clientService.IsAdmin() || clientService.GetUserId() == context.Source.Id)
                    {
                        if (!schemaValidation.GetValidatableArgument(context, "pagedArray", out NewPagedArray pagedArray))
                        {
                            return null;
                        }
                        return repository.GetPage(pagedArray.Page, pagedArray.PerPage, repository.GetPublishedJokes(context.Source.Id));
                    }
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;
                }
            );
        }
    }
}
