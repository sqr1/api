﻿using GraphQL;
using GraphQL.DataLoader;
using GraphQL.SystemTextJson;
using AudioJokeAPI.DataAccess;
using AudioJokeAPI.ErrorHandling;
using AudioJokeAPI.Models;
using AudioJokeAPI.Schema;
using AudioJokeAPI.Services;
using AudioJokeAPI.Utils;
using AudioJokeAPITest.Helpers;
using AudioJokeAPITest.Models;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Text;

namespace AudioJokeAPITest.Schema
{
    public class MutationObjectTest
    {
        private MainSchema _schema;

        private Mock<IDataLoaderContextAccessor> _dataLoaderContextAccessor;

        private Mock<IDataLoaderService> _dataLoaderService;

        private Mock<IRepository> _repository;

        private Mock<IClientService> _clientService;

        private Mock<ICDNService> _cdnService;

        private Mock<IGoogleAuthenticationService> _googleAuthenticationService;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<IRepository>();
            _dataLoaderContextAccessor = new Mock<IDataLoaderContextAccessor>();
            _dataLoaderService = new Mock<IDataLoaderService>();
            _clientService = new Mock<IClientService>();
            _cdnService = new Mock<ICDNService>();
            _googleAuthenticationService = new Mock<IGoogleAuthenticationService>();

            var services = new ServiceCollection();

            services.AddSingleton(_dataLoaderContextAccessor.Object);
            services.AddSingleton(_dataLoaderService.Object);
            services.AddSingleton(_repository.Object);
            services.AddSingleton(_clientService.Object);
            services.AddSingleton(_cdnService.Object);
            services.AddSingleton(_googleAuthenticationService.Object);

            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<ISchemaValidation, SchemaValidation>();
            services.AddSingleton<DataLoaderDocumentListener>();
            services.AddSingleton<QueryObject>();
            services.AddSingleton<MutationObject>();
            services.AddGraphQLModels();

            _schema = new MainSchema(services.BuildServiceProvider());
        }

        // SIGN IN VIA GOOGLE

        [TestCase("t")]
        [TestCase("tOkEn12472!@#$%^&*(){}'`")]
        [TestCase("   token   ")]
        public void TestSignInViaGoogleSuccess(string token)
        {
            var sessionId = Guid.NewGuid();
            const string email = "user@gmail.com";
            const string ip = "127.0.0.1";
            var accessToken = Guid.NewGuid().ToString();
            var authenticationResult = new GoogleAuthenticationResult
            {
                Email = email
            };

            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetHost()).Returns(ip);

            _repository.Reset();
            _repository.Setup(m => m.CreateSession(email, ip)).Returns(new Session { Id = sessionId, AccessToken = accessToken });

            _googleAuthenticationService.Setup(m => m.Verify(token, out authenticationResult)).Returns(true);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ signInViaGoogle(token: \"{token}\") {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);

            Assert.AreEqual(null, result.Errors);
            Assert.AreEqual(sessionId, result.Data.SignInViaGoogle.Id);

            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Once());
            _repository.Verify(m => m.CreateSession(email, ip), Times.Once());
            _googleAuthenticationService.Verify(m => m.Verify(token, out authenticationResult), Times.Once());
        }

        [Test]
        public void TestSignInViaGoogleForbiddenFail()
        {
            var sessionId = Guid.NewGuid();
            const string token = "token";
            const string email = "user@gmail.com";
            const string ip = "127.0.0.1";
            var accessToken = Guid.NewGuid().ToString();
            var authenticationResult = new GoogleAuthenticationResult
            {
                Email = email
            };

            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetHost()).Returns(ip);

            _repository.Reset();
            _repository.Setup(m => m.CreateSession(email, ip)).Returns(new Session { Id = sessionId, AccessToken = accessToken });

            _googleAuthenticationService.Setup(m => m.Verify(token, out authenticationResult)).Returns(false);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ signInViaGoogle(token: \"{token}\") {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);

            Assert.IsNull(result.Data.SignInViaGoogle);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);

            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _repository.Verify(m => m.CreateSession(email, ip), Times.Never());
            _googleAuthenticationService.Verify(m => m.Verify(token, out authenticationResult), Times.Once());
        }

        [Test]
        public void TestSignInViaGoogleUnknownFail()
        {
            const string token = "token";
            const string email = "user@gmail.com";
            const string ip = "127.0.0.1";
            var accessToken = Guid.NewGuid().ToString();
            var authenticationResult = new GoogleAuthenticationResult
            {
                Email = email
            };

            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetHost()).Returns(ip);

            _repository.Reset();
            _repository.Setup(m => m.CreateSession(email, ip)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            _googleAuthenticationService.Setup(m => m.Verify(token, out authenticationResult)).Returns(true);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ signInViaGoogle(token: \"{token}\") {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);

            Assert.IsNull(result.Data.SignInViaGoogle);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);

            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _repository.Verify(m => m.CreateSession(email, ip), Times.Once());
            _googleAuthenticationService.Verify(m => m.Verify(token, out authenticationResult), Times.Once());
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase("  ")]
        public void TestSignInViaGoogleValidationFail(string token)
        {
            const string email = "user@gmail.com";
            const string ip = "127.0.0.1";
            var accessToken = Guid.NewGuid().ToString();
            var authenticationResult = new GoogleAuthenticationResult
            {
                Email = email
            };

            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetHost()).Returns(ip);

            _repository.Reset();
            _repository.Setup(m => m.CreateSession(email, ip)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            _googleAuthenticationService.Setup(m => m.Verify(token, out authenticationResult)).Returns(true);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ signInViaGoogle(token: \"{token}\") {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);

            Assert.IsNull(result.Data.SignInViaGoogle);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_VALIDATION);

            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _repository.Verify(m => m.CreateSession(email, ip), Times.Never());
            _googleAuthenticationService.Verify(m => m.Verify(token, out authenticationResult), Times.Never());
        }

        // REFRESH SESSION

        [Test]
        public void TestRefreshSessionSuccess()
        {
            var sessionId = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();
            var newAccessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.SetAccessToken(newAccessToken));

            _repository.Reset();
            _repository.Setup(m => m.Refresh(accessToken)).Returns(new Session { Id = sessionId, AccessToken = newAccessToken });

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ refreshSession {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Errors);
            Assert.AreEqual(sessionId, result.Data.RefreshSession.Id);
            _repository.Verify(m => m.Refresh(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.SetAccessToken(newAccessToken), Times.Once());
        }

        [Test]
        public void TestRefreshSessionForbiddenFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.SetAccessToken(accessToken));

            _repository.Reset();
            _repository.Setup(m => m.Refresh(accessToken)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ refreshSession {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.RefreshSession);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _repository.Verify(m => m.Refresh(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
        }

        [Test]
        public void TestRefreshSessionUnauthorizedFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.SetAccessToken(accessToken));

            _repository.Reset();
            _repository.Setup(m => m.Refresh(accessToken)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ refreshSession {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.RefreshSession);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _repository.Verify(m => m.Refresh(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
        }

        [Test]
        public void TestRefreshSessionUnknownFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.SetAccessToken(accessToken));

            _repository.Reset();
            _repository.Setup(m => m.Refresh(accessToken)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ refreshSession {{ id }} }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.RefreshSession);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
        }

        // LOGOUT

        [Test]
        public void TestLogoutSuccess()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.Logout(accessToken));

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ logout }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Errors);
            Assert.AreEqual("success", result.Data.Logout);
            _repository.Verify(m => m.Logout(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
        }

        [Test]
        public void TestLogoutForbiddenFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.Logout(accessToken)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ logout }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.Logout);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _repository.Verify(m => m.Logout(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
        }

        [Test]
        public void TestLogoutUnknownFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.Logout(accessToken)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ logout }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.Logout);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _repository.Verify(m => m.Logout(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
        }

        [Test]
        public void TestLogoutUnauthorizedFail()
        {
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.Logout(accessToken)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ logout }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.Logout);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _repository.Verify(m => m.Logout(accessToken), Times.Once());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
        }

        // REGISTER USER

        [TestCase("nickname")]
        [TestCase("n.ickname")]
        [TestCase("n_ickname")]
        [TestCase("n_ick4na2me")]
        public void TestRegisterUserSuccess(string nickname)
        {
            var accessToken = Guid.NewGuid().ToString();
            var user = new User
            {
                Id = Guid.NewGuid(),
                Nickname = nickname,
                Bio = "",
                IsAdmin = false,
                IsRegistered = true,
                CreatedAt = DateTime.Now,
                Email = "user@gmail.com"
            };

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.RegisterUser(accessToken, nickname)).Returns(user);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ registerUser(user: {{nickname: \"{nickname}\" }}) {{ id nickname}}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Errors);
            Assert.AreEqual(user.Id, result.Data.RegisterUser.Id);
            Assert.AreEqual(user.Nickname, result.Data.RegisterUser.Nickname);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.RegisterUser(accessToken, nickname), Times.Once());
        }

        [TestCase("Nickname")]
        [TestCase("NICKNAME")]
        [TestCase("n&ickname")]
        [TestCase("никнейм")]
        public void TestRegisterUserValidationFail(string nickname)
        {
            var accessToken = Guid.NewGuid().ToString();
            var user = new User
            {
                Id = Guid.NewGuid(),
                Nickname = nickname,
                Bio = "",
                IsAdmin = false,
                IsRegistered = true,
                CreatedAt = DateTime.Now,
                Email = "user@gmail.com"
            };

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.RegisterUser(accessToken, nickname)).Returns(user);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ registerUser(user: {{nickname: \"{nickname}\" }}) {{ id nickname}}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.RegisterUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_VALIDATION);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Never());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.RegisterUser(accessToken, nickname), Times.Never());
        }

        [Test]
        public void TestRegisterUserForbiddenFail()
        {
            var nickname = "nickname";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.RegisterUser(accessToken, nickname)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ registerUser(user: {{nickname: \"{nickname}\" }}) {{ id nickname}}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.RegisterUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.RegisterUser(accessToken, nickname), Times.Once());
        }

        [Test]
        public void TestRegisterUserUnauthorizedFail()
        {
            var nickname = "nickname";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.RegisterUser(accessToken, nickname)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ registerUser(user: {{nickname: \"{nickname}\" }}) {{ id nickname}}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.RegisterUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
            _repository.Verify(m => m.RegisterUser(accessToken, nickname), Times.Once());
        }

        [Test]
        public void TestRegisterUserUnknownFail()
        {
            var nickname = "nickname";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.RegisterUser(accessToken, nickname)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ registerUser(user: {{nickname: \"{nickname}\" }}) {{ id nickname}}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.RegisterUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.RegisterUser(accessToken, nickname), Times.Once());
        }

        // EDIT USER

        [TestCase(null, "bio")]
        [TestCase("nickname", null)]
        [TestCase("nickname", "bio")]
        [TestCase("n.ickname", "b")]
        [TestCase("n_ickname", "BIO")]
        [TestCase("n_ick4na2me", "B_I_Ojoewpoefwefo wepfowep")]
        [TestCase("nickname", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void TestEditUserSuccess(string nickname, string bio)
        {
            var id = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.EditUser(accessToken, id, nickname, bio));

            StringBuilder query = new StringBuilder();
            query.Append($"mutation {{ editUser(user: {{id: \"{id.ToString()}\" ");
            if (nickname != null)
            {
                query.Append($"nickname: \"{nickname}\" ");
            }
            if (bio != null)
            {
                query.Append($"bio: \"{bio}\" ");
            }

            query.Append("})}");

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = query.ToString();
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Errors);
            Assert.AreEqual("success", result.Data.EditUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.EditUser(accessToken, id, nickname, bio), Times.Once());
        }

        [TestCase(null, null)]
        [TestCase("Nickname", null)]
        [TestCase("NICKNAME", null)]
        [TestCase("n&ickname", null)]
        [TestCase("никнейм", null)]
        [TestCase("Nickname", "bio")]
        [TestCase("NICKNAME", "bio")]
        [TestCase("n&ickname", "bio")]
        [TestCase("никнейм", "bio")]
        [TestCase("nickname", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void TestEditUserValidationFail(string nickname, string bio)
        {
            var id = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.EditUser(accessToken, id, nickname, bio));

            StringBuilder query = new StringBuilder();
            query.Append($"mutation {{ editUser(user: {{id: \"{id.ToString()}\" ");
            if (nickname != null)
            {
                query.Append($"nickname: \"{nickname}\" ");
            }
            if (bio != null)
            {
                query.Append($"bio: \"{bio}\" ");
            }

            query.Append("})}");

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = query.ToString();
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_VALIDATION);
            Assert.AreEqual(null, result.Data.EditUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Never());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.EditUser(accessToken, id, nickname, bio), Times.Never());
        }

        [Test]
        public void TestEditUserForbiddenFail()
        {
            var id = Guid.NewGuid();
            var nickname = "nickname";
            var bio = "bio";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.EditUser(accessToken, id, nickname, bio)).Throws(PostgresExceptions.PostgresExceptionCustom);

            StringBuilder query = new StringBuilder();
            query.Append($"mutation {{ editUser(user: {{id: \"{id.ToString()}\" ");
            query.Append($"nickname: \"{nickname}\" ");
            query.Append($"bio: \"{bio}\" ");
            query.Append("})}");

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = query.ToString();
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            Assert.AreEqual(null, result.Data.EditUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.EditUser(accessToken, id, nickname, bio), Times.Once());
        }

        [Test]
        public void TestEditUserUnauthorizedFail()
        {
            var id = Guid.NewGuid();
            var nickname = "nickname";
            var bio = "bio";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.EditUser(accessToken, id, nickname, bio)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            StringBuilder query = new StringBuilder();
            query.Append($"mutation {{ editUser(user: {{id: \"{id.ToString()}\" ");
            query.Append($"nickname: \"{nickname}\" ");
            query.Append($"bio: \"{bio}\" ");
            query.Append("})}");

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = query.ToString();
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            Assert.AreEqual(null, result.Data.EditUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
            _repository.Verify(m => m.EditUser(accessToken, id, nickname, bio), Times.Once());
        }

        [Test]
        public void TestEditUserUnknownFail()
        {
            var id = Guid.NewGuid();
            var nickname = "nickname";
            var bio = "bio";
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.EditUser(accessToken, id, nickname, bio)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            StringBuilder query = new StringBuilder();
            query.Append($"mutation {{ editUser(user: {{id: \"{id.ToString()}\" ");
            query.Append($"nickname: \"{nickname}\" ");
            query.Append($"bio: \"{bio}\" ");
            query.Append("})}");

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = query.ToString();
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            Assert.AreEqual(null, result.Data.EditUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.EditUser(accessToken, id, nickname, bio), Times.Once());
        }

        // DELETE USER

        [Test]
        public void TestDeleteUserSuccess()
        {
            var accessToken = Guid.NewGuid().ToString();
            var userId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteUser(accessToken, userId));

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteUser(userId: \"{userId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Errors);
            Assert.AreEqual("success", result.Data.DeleteUser);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteUser(accessToken, userId), Times.Once());
        }

        [Test]
        public void TestDeleteUserForbiddenFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var userId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteUser(accessToken, userId)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteUser(userId: \"{userId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.DeleteUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteUser(accessToken, userId), Times.Once());
        }

        [Test]
        public void TestDeleteUserUnauthorizedFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var userId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteUser(accessToken, userId)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteUser(userId: \"{userId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.DeleteUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
            _repository.Verify(m => m.DeleteUser(accessToken, userId), Times.Once());
        }

        [Test]
        public void TestDeleteUserUnknownFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var userId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteUser(accessToken, userId)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteUser(userId: \"{userId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.AreEqual(null, result.Data.DeleteUser);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteUser(accessToken, userId), Times.Once());
        }

        // CREATE JOKE

        
        // DELETE POST

        [Test]
        public void TestDeleteJokeSuccess()
        {
            var accessToken = Guid.NewGuid().ToString();
            var jokeId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteJoke(accessToken, jokeId));

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteJoke(jokeId: \"{jokeId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Errors);
            Assert.AreEqual("success", result.Data.DeleteJoke);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteJoke(accessToken, jokeId), Times.Once());
        }

        [Test]
        public void TestDeleteJokeForbidden()
        {
            var accessToken = Guid.NewGuid().ToString();
            var jokeId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteJoke(accessToken, jokeId)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteJoke(jokeId: \"{jokeId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            Assert.IsNull(result.Data.DeleteJoke);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteJoke(accessToken, jokeId), Times.Once());
        }

        [Test]
        public void TestDeleteJokeUnknown()
        {
            var accessToken = Guid.NewGuid().ToString();
            var jokeId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteJoke(accessToken, jokeId)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteJoke(jokeId: \"{jokeId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            Assert.IsNull(result.Data.DeleteJoke);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteJoke(accessToken, jokeId), Times.Once());
        }

        // CREATE REACTION

        [TestCase(ReactionType.dislike)]
        [TestCase(ReactionType.like)]
        [TestCase(ReactionType.none)]
        [TestCase(ReactionType.report)]
        public void TestCreateReactionSuccess(ReactionType reactionType)
        {
            var jokeId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();
            var reaction = new Reaction
            {
                Id = Guid.NewGuid(),
                JokeId = jokeId,
                UserId = userId
            };

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.CreateReaction(accessToken, jokeId, reactionType)).Returns(reaction);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ createReaction(reaction: {{jokeId: \"{jokeId}\" type: {reactionType.ToString().GetUpperCaseUnderscoreStyleName()}}}) {{ id }}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Errors);
            Assert.AreEqual(reaction.Id, result.Data.CreateReaction.Id);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.CreateReaction(accessToken, jokeId, reactionType), Times.Once());
        }

        [TestCase(ReactionType.dislike)]
        [TestCase(ReactionType.like)]
        [TestCase(ReactionType.none)]
        [TestCase(ReactionType.report)]
        public void TestCreateReactionForbiddenFail(ReactionType reactionType)
        {
            var jokeId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();
            var reaction = new Reaction
            {
                Id = Guid.NewGuid(),
                JokeId = jokeId,
                UserId = userId
            };

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.CreateReaction(accessToken, jokeId, reactionType)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ createReaction(reaction: {{jokeId: \"{jokeId}\" type: {reactionType.ToString().GetUpperCaseUnderscoreStyleName()}}}) {{ id }}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.CreateReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.CreateReaction(accessToken, jokeId, reactionType), Times.Once());
        }

        [TestCase(ReactionType.dislike)]
        [TestCase(ReactionType.like)]
        [TestCase(ReactionType.none)]
        [TestCase(ReactionType.report)]
        public void TestCreateReactionUnauthorizedFail(ReactionType reactionType)
        {
            var jokeId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.CreateReaction(accessToken, jokeId, reactionType)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ createReaction(reaction: {{jokeId: \"{jokeId}\" type: {reactionType.ToString().GetUpperCaseUnderscoreStyleName()}}}) {{ id }}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.CreateReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
            _repository.Verify(m => m.CreateReaction(accessToken, jokeId, reactionType), Times.Once());
        }

        [TestCase(ReactionType.dislike)]
        [TestCase(ReactionType.like)]
        [TestCase(ReactionType.none)]
        [TestCase(ReactionType.report)]
        public void TestCreateReactionUnknownFail(ReactionType reactionType)
        {
            var jokeId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var accessToken = Guid.NewGuid().ToString();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.CreateReaction(accessToken, jokeId, reactionType)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ createReaction(reaction: {{jokeId: \"{jokeId}\" type: {reactionType.ToString().GetUpperCaseUnderscoreStyleName()}}}) {{ id }}}}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.CreateReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.CreateReaction(accessToken, jokeId, reactionType), Times.Once());
        }

        // DELETE REACTION

        [Test]
        public void TestDeleteReactionSuccess()
        {
            var accessToken = Guid.NewGuid().ToString();
            var reactionId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteReaction(accessToken, reactionId));

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteReaction(reactionId: \"{reactionId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Errors);
            Assert.AreEqual("success", result.Data.DeleteReaction);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteReaction(accessToken, reactionId), Times.Once());
        }

        [Test]
        public void TestDeleteReactionForbiddenFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var reactionId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteReaction(accessToken, reactionId)).Throws(PostgresExceptions.PostgresExceptionCustom);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteReaction(reactionId: \"{reactionId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.DeleteReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_FORBIDDEN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteReaction(accessToken, reactionId), Times.Once());
        }

        [Test]
        public void TestDeleteReactionUnauthorizedFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var reactionId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteReaction(accessToken, reactionId)).Throws(PostgresExceptions.PostgresExceptionUnauthorized);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteReaction(reactionId: \"{reactionId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.DeleteReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNAUTHORIZED);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Once());
            _repository.Verify(m => m.DeleteReaction(accessToken, reactionId), Times.Once());
        }

        [Test]
        public void TestDeleteReactionUnknownFail()
        {
            var accessToken = Guid.NewGuid().ToString();
            var reactionId = Guid.NewGuid();

            _clientService.Reset();
            _clientService.Setup(m => m.SetAccessToken(accessToken));
            _clientService.Setup(m => m.GetAccessToken()).Returns(accessToken);
            _clientService.Setup(m => m.DeleteAccessToken());

            _repository.Reset();
            _repository.Setup(m => m.DeleteReaction(accessToken, reactionId)).Throws(PostgresExceptions.PostgresExceptionUnknown);

            var json = _schema.ExecuteAsync(_ =>
            {
                _.Query = $"mutation {{ deleteReaction(reactionId: \"{reactionId}\") }}";
            }).Result;

            var result = JsonConvert.DeserializeObject<GraphQLRequestResult<GraphQLMutationResult>>(json);
            Assert.IsNull(result.Data.DeleteReaction);
            result.VerifyHasExactlyOneError(GraphQLErrorHandler.ERROR_UNKNOWN);
            _clientService.Verify(m => m.SetAccessToken(accessToken), Times.Never());
            _clientService.Verify(m => m.GetAccessToken(), Times.Once());
            _clientService.Verify(m => m.DeleteAccessToken(), Times.Never());
            _repository.Verify(m => m.DeleteReaction(accessToken, reactionId), Times.Once());
        }
    }
}
