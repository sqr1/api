using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using AudioJokeAPI.Cron;
using AudioJokeAPI.Cron.Jobs;
using AudioJokeAPI.DataAccess;
using AudioJokeAPI.Schema;
using AudioJokeAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace AudioJokeAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<IDataLoaderContextAccessor, DataLoaderContextAccessor>();
            services.AddSingleton<DataLoaderDocumentListener>();
            services.AddSingleton<ICDNService>(new CDNService(
                Configuration.GetSection("CDN")["Url"],
                Configuration.GetSection("CDN")["Bucket"],
                Configuration.GetSection("CDN")["AccessKey"],
                Configuration.GetSection("CDN")["SecretKey"]
            ));
            services.AddSingleton<ISchemaValidation, SchemaValidation>();
            services.AddSingleton<IGoogleAuthenticationService>(new GoogleAuthenticationService(Configuration.GetSection("GoogleAuthentication")["ClientId"]));

            services.AddCronJob<CleanUpSessions>(config =>
            {
                config.TimeZoneInfo = TimeZoneInfo.Local;
                config.CronExpression = Configuration.GetSection("Cron")["CleanUpExpiredSessions"];
            });
            services.AddCronJob<CleanUpUsers>(config =>
            {
                config.TimeZoneInfo = TimeZoneInfo.Local;
                config.CronExpression = Configuration.GetSection("Cron")["CleanUpExpiredUsers"];
            });

            services.AddDbContextPool<PostgresContext>(options => options.UseNpgsql(Configuration.GetConnectionString("Database")));
            services.AddScoped<IRepository, Repository>();

            services.AddScoped<IDataLoaderService, DataLoaderService>();
            services.AddScoped<ISchema, MainSchema>();
            services.AddScoped<QueryObject>();
            services.AddScoped<MutationObject>();
            services.AddGraphQLModels();

            services.AddScoped<IClientService, ClientService>();

            services.AddHttpContextAccessor();
            services.AddCors();
            services.AddGraphQL().AddSystemTextJson().AddErrorInfoProvider(options => options.ExposeExceptionStackTrace = true).AddDataLoader();
            services.AddLogging(builder => builder.AddConsole());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseGraphQL<ISchema>();

            if (env.IsDevelopment())
            {
                app.UseGraphQLPlayground(new GraphQLPlaygroundOptions
                { Path = "/", RequestCredentials = RequestCredentials.SameOrigin });
            }
        }
    }
}
