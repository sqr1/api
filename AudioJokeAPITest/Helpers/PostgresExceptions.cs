﻿using AudioJokeAPI.ErrorHandling;

namespace AudioJokeAPITest.Helpers
{
    public static class PostgresExceptions
    {
        public static readonly Npgsql.PostgresException PostgresExceptionCustom =
            new Npgsql.PostgresException("CREATE_SOMETHING_001", "ERROR", "ERROR", "P0001");

        public static readonly Npgsql.PostgresException PostgresExceptionUnknown =
            new Npgsql.PostgresException("CONNECTION_FAILURE", "ERROR", "ERROR", "08006");

        public static readonly Npgsql.PostgresException PostgresExceptionTooManyAttempts =
            new Npgsql.PostgresException(GraphQLErrorHandler.POSTGRES_TOO_MANY_ATTEMPTS_EXCEPTION, "ERROR", "ERROR", "P0001");

        public static readonly Npgsql.PostgresException PostgresExceptionUnauthorized =
            new Npgsql.PostgresException(GraphQLErrorHandler.POSTGRES_UNAUTHORIZED_EXCEPTION, "ERROR", "ERROR", "P0001");
    }
}
