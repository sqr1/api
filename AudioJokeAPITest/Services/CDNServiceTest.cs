﻿using System;
using System.IO;
using AudioJokeAPI.Services;
using AudioJokeAPITest.Helpers;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace AudioJokeAPITest.Services
{
    public class CDNServiceTest
    {
        private IConfiguration _configuration;

        [SetUp]
        public void SetUp()
        {
            _configuration = ConfigurationManager.Build();
        }

        [Test]
        public void TestCDNServiceSuccess()
        {
            var cdnService = new CDNService(
                _configuration.GetSection("CDN")["Url"],
                _configuration.GetSection("CDN")["Bucket"],
                _configuration.GetSection("CDN")["AccessKey"],
                _configuration.GetSection("CDN")["SecretKey"]);
            var id = Guid.NewGuid();
            Assert.IsTrue(cdnService.UploadAudioToCDN(FileLoader.LoadFile("file.mp3"), id));
            Assert.IsTrue(cdnService.DeleteAudioFromCDN(id));
        }
    }
}
