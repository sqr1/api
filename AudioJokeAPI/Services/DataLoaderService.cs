﻿using GraphQL.DataLoader;
using AudioJokeAPI.DataAccess;
using System;

namespace AudioJokeAPI.Services
{
    public class DataLoaderService : IDataLoaderService
    {
        private readonly IDataLoaderContextAccessor _accessor;

        private readonly IRepository _repository;

        public DataLoaderService(IDataLoaderContextAccessor accessor, IRepository repository)
        {
            _accessor = accessor;
            _repository = repository;
        }

        public IDataLoaderResult<T> GetById<T>(Guid id) where T : BaseModel<T>
        {
            return _accessor.Context.GetOrAddBatchLoader<Guid, T>(typeof(T).Name + "ById", _repository.GetModelsByIdAsync<T>).LoadAsync(id);
        }
    }
}
