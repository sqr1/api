﻿using GraphQL.Types;
using AudioJokeAPI.Models;
using AudioJokeAPI.Services;

namespace AudioJokeAPI.Schema.Types
{
    public sealed class SessionObject : ObjectGraphType<Session>
    {
        public SessionObject(IDataLoaderService dataLoaderService, IClientService clientService)
        {
            Name = "Session";
            Field("id", x => x.Id);
            Field<NonNullGraphType<UserObject>>("user",
                resolve: context => dataLoaderService.GetById<User>(context.Source.UserId));
            Field("ip", x => x.Ip);
            Field("accessToken", x => x.AccessToken);
            Field("accessTokenExpiryDate", x => x.AccessTokenExpiryDate);
        }
    }
}
