﻿using AudioJokeAPI.Schema;
using System;

namespace AudioJokeAPI.Models.Inputs
{
    public class NewJoke : IGraphQLValidatable
    {
        public string Text { get; set; }

        public bool IsValid(IServiceProvider serviceProvider)
        {
            return !string.IsNullOrWhiteSpace(Text);
        }
    }
}
