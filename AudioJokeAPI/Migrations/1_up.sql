﻿CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TYPE "reaction_type" AS ENUM (
  'like',
  'none',
  'dislike',
  'report'
);

CREATE TABLE "migrations"
(
    "id"           UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "created_at"   timestamptz DEFAULT (now()),
    "version"      integer UNIQUE NOT NULL
);

CREATE TABLE "users" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created_at" timestamptz DEFAULT (now()),
  "email" varchar UNIQUE NOT NULL,
  "is_admin" boolean NOT NULL DEFAULT FALSE,
  "is_registered" boolean NOT NULL DEFAULT FALSE,
  "nickname" varchar NOT NULL DEFAULT '',
  "bio" varchar NOT NULL DEFAULT ''
);

CREATE TABLE "sessions" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created_at" timestamptz DEFAULT (now()),
  "user_id" UUID NOT NULL,
  "ip" varchar NOT NULL,
  "access_token" varchar UNIQUE NOT NULL,
  "access_token_expiry_date" timestamptz NOT NULL
);

CREATE TABLE "jokes" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created_at" timestamptz DEFAULT (now()),
  "user_id" UUID NOT NULL,
  "text" varchar NOT NULL
);

CREATE TABLE "reactions" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created_at" timestamptz DEFAULT (now()),
  "user_id" UUID NOT NULL,
  "joke_id" UUID NOT NULL,
  "type" reaction_type NOT NULL
);

ALTER TABLE "sessions" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;

ALTER TABLE "jokes" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;

ALTER TABLE "reactions" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;

ALTER TABLE "reactions" ADD FOREIGN KEY ("joke_id") REFERENCES "jokes" ("id") ON DELETE CASCADE;

