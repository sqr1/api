﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.IO;

namespace AudioJokeAPI.Services
{
    public class CDNService : ICDNService
    {
        private readonly IAmazonS3 _client;

        private readonly string _url;

        private readonly string _bucket;

        public CDNService(string url, 
            string bucket,
            string accessKey,
            string secretKey)
        {
            _url = url;
            _bucket = bucket;
            _client = new AmazonS3Client(
                new BasicAWSCredentials(accessKey, secretKey), 
                new AmazonS3Config { ServiceURL = url });

        }

        public bool UploadAudioToCDN(byte[] data, Guid id)
        {
            if (!UploadFileToCDN(data, $"{id}.mp3"))
                return false;
            return true;
        }

        public bool DeleteAudioFromCDN(Guid id)
        {
            if (!DeleteFileFromCDN($"{id}.mp3"))
                return false;
            return true;
        }

        public string GetAudioUrl(Guid id)
        {
            return $"https://{_bucket}.{_url.Split("//", 2)[1]}/{id}.mp3";
        }

        private bool UploadFileToCDN(byte[] file, string name)
        {
            var transferUtility = new TransferUtility(_client);
            var memory = new MemoryStream();
            var fileTransferUtilityRequest = new TransferUtilityUploadRequest
            {
                BucketName = _bucket,
                InputStream = memory,
                StorageClass = S3StorageClass.Standard,
                Key = name,
                CannedACL = S3CannedACL.PublicRead
            };
            memory.Write(file);
            try
            {
                transferUtility.UploadAsync(fileTransferUtilityRequest).Wait();
            }
            catch (AggregateException)
            {
                return false;
            }
            memory.Close();
            return true;
        }

        private bool DeleteFileFromCDN(string name)
        {
            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = _bucket,
                Key = name
            };
            try
            {
                _client.DeleteObjectAsync(deleteObjectRequest).Wait();
            }
            catch (AggregateException)
            {
                return false;
            }
            return true;
        }
    }
}
