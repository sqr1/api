CREATE OR REPLACE FUNCTION delete_reaction(_token varchar, _reaction_id uuid) RETURNS BOOLEAN AS $$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    reaction_ Reactions%ROWTYPE;
BEGIN
    SELECT * INTO reaction_ FROM reactions WHERE id = _reaction_id LIMIT 1;
    IF reaction_ IS NULL THEN
        RAISE EXCEPTION 'DELETE_REACTION_001' USING HINT = 'Invalid reaction id';
    END IF;

    IF session_.user_id <> reaction_.user_id
    THEN
        IF (SELECT is_admin FROM is_admin(session_.user_id) LIMIT 1)
        THEN
            RAISE EXCEPTION 'DELETE_REACTION_002' USING HINT = 'Forbidden';
        END IF;
    END IF;

    DELETE FROM reactions WHERE id = reaction_.id;

    RETURN true;
END;
$$ LANGUAGE plpgsql;
