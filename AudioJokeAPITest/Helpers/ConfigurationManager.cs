﻿using Microsoft.Extensions.Configuration;

namespace AudioJokeAPITest.Helpers
{
    public static class ConfigurationManager
    {
        public static IConfiguration Build()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.test.json")
                .Build();
        }
    }
}
