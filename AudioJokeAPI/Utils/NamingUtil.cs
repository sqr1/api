﻿using System.Linq;

namespace AudioJokeAPI.Utils
{
    public static class NamingUtil
    {
        private static string GetUnderscoreStyleName(string value)
        {
            return string.Concat(value.Select((x, i) =>
            {
                var isPrevCharLower = i != 0 && char.IsLower(value[i - 1]);
                var isPrevCharNumber = i != 0 && char.IsNumber(value[i - 1]);
                return isPrevCharLower && (char.IsUpper(x) || char.IsNumber(x))
                    || isPrevCharNumber && (char.IsUpper(x))
                    ? "_" + x : x.ToString();
            }));
        }

        public static string GetUpperCaseUnderscoreStyleName(this string value)
        {
            return GetUnderscoreStyleName(value).ToUpperInvariant();
        }
    }
}
