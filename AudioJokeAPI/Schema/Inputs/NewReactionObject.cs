﻿using GraphQL.Types;
using AudioJokeAPI.Models.Inputs;
using AudioJokeAPI.Schema.Types;

namespace AudioJokeAPI.Schema.Inputs
{
    public class NewReactionObject : InputObjectGraphType<NewReaction>
    {
        public NewReactionObject()
        {
            Name = "NewReaction";
            Field("jokeId", x => x.JokeId);
            Field<NonNullGraphType<ReactionTypeObject>>("type");
        }
    }
}
