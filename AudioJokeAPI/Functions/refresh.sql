CREATE OR REPLACE FUNCTION refresh(_token varchar) RETURNS sessions
    LANGUAGE plpgsql
AS
$$
DECLARE
    session_ sessions%ROWTYPE := check_session(_token);
BEGIN
    session_.access_token = uuid_generate_v4();
    session_.access_token_expiry_date := now() + CONSTANT_ACCESS_TOKEN_DURATION();
    UPDATE sessions SET (access_token, access_token_expiry_date) = (session_.access_token, session_.access_token_expiry_date) WHERE id = session_.id;

    RETURN session_;
END;
$$;
