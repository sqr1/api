﻿using AudioJokeAPI.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;

namespace AudioJokeAPI.DataAccess
{
    public partial class PostgresContext : DbContext
    {
        public PostgresContext(DbContextOptions<PostgresContext> options)
            : base(options)
        {
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ReactionType>("public.reaction_type");
        }

        public virtual DbSet<Joke> Jokes { get; set; }
        public virtual DbSet<Reaction> Reactions { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                throw new Exception("Options are not configured");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresEnum<ReactionType>("public", "reaction_type");
            modelBuilder.HasPostgresExtension("adminpack");

            modelBuilder.Entity<Joke>(entity =>
            {
                entity.ToTable("jokes");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Text)
                    .HasColumnName("text")
                    .HasColumnType("character varying");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Jokes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("jokes_user_id_fkey");
            });

            modelBuilder.Entity<Reaction>(entity =>
            {
                entity.ToTable("reactions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.JokeId).HasColumnName("joke_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Joke)
                    .WithMany(p => p.Reactions)
                    .HasForeignKey(d => d.JokeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("reactions_post_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Reactions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("reactions_user_id_fkey");
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.ToTable("sessions");

                entity.HasIndex(e => e.AccessToken)
                    .HasName("sessions_access_token_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccessToken)
                    .HasColumnName("access_token")
                    .HasColumnType("character varying");

                entity.Property(e => e.AccessTokenExpiryDate).HasColumnName("access_token_expiry_date");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Ip)
                    .HasColumnName("ip")
                    .HasColumnType("character varying");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Sessions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sessions_user_id_fkey");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Email)
                    .HasName("users_email_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bio)
                    .HasColumnName("bio")
                    .HasColumnType("character varying");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.IsAdmin).HasColumnName("is_admin");
                entity.Property(e => e.IsRegistered).HasColumnName("is_registered");

                entity.Property(e => e.Nickname)
                    .HasColumnName("nickname")
                    .HasColumnType("character varying");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("character varying");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
