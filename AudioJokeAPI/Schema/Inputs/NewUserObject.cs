﻿using GraphQL.Types;
using AudioJokeAPI.Models.Inputs;

namespace AudioJokeAPI.Schema.Inputs
{
    public sealed class NewUserObject : InputObjectGraphType<NewUser>
    {
        public NewUserObject()
        {
            Name = "NewUser";
            Field("nickname", x => x.Nickname);
        }
    }
}
