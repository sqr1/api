﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AudioJokeAPI.DataAccess;
using Microsoft.Extensions.DependencyInjection;

namespace AudioJokeAPI.Cron.Jobs
{
    public class CleanUpSessions : CronJobService
    {
        private readonly IServiceProvider _serviceProvider;

        public CleanUpSessions(
            IServiceProvider serviceProvider,
            IScheduleConfig<CleanUpSessions> config)
            : base(config.CronExpression, config.TimeZoneInfo)
        {
            _serviceProvider = serviceProvider;
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();
            var repository = scope.ServiceProvider.GetRequiredService<IRepository>();
            repository.DeleteExpiredSesssions();
            return Task.CompletedTask;
        }
    }
}
