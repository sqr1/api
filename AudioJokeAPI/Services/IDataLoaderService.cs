﻿using GraphQL.DataLoader;
using System;

namespace AudioJokeAPI.Services
{
    public interface IDataLoaderService
    {
        public IDataLoaderResult<T> GetById<T>(Guid id) where T : BaseModel<T>;
    }
}
