﻿using GraphQL.Utilities;
using System;

namespace AudioJokeAPI.Schema
{
    public class MainSchema : GraphQL.Types.Schema
    {
        public MainSchema(IServiceProvider provider) : base(provider)
        {
            Query = provider.GetRequiredService<QueryObject>();
            Mutation = provider.GetRequiredService<MutationObject>();
        }
    }
}
