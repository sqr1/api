﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace AudioJokeAPI.Services
{
    public class GoogleAuthenticationService: IGoogleAuthenticationService
    {
        private readonly string _clientId;

        public GoogleAuthenticationService(string clientId)
        {
            _clientId = clientId;
        }

        public bool Verify(string token, out GoogleAuthenticationResult result)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://oauth2.googleapis.com/tokeninfo?id_token=" + token);
            string resultString;
            try
            {
                using var response = (HttpWebResponse) request.GetResponse();
                using var stream = response.GetResponseStream();
                if (stream == null)
                {
                    result = new GoogleAuthenticationResult();
                    return false;
                }
                using var reader = new StreamReader(stream);
                resultString = reader.ReadToEnd();
            }
            catch (WebException)
            {
                result = new GoogleAuthenticationResult();
                return false;
            }
            result = JsonConvert.DeserializeObject<GoogleAuthenticationResult>(resultString);
            return result.Aud == _clientId &&
                   (result.Iss == "accounts.google.com" || result.Iss == "https://accounts.google.com") &&
                   result.Exp != null && Convert.ToInt64(result.Exp) > DateTimeOffset.Now.ToUnixTimeSeconds();
        }
    }
}
