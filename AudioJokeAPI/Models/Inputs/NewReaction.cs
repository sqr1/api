﻿using AudioJokeAPI.Schema;
using System;

namespace AudioJokeAPI.Models.Inputs
{
    public class NewReaction : IGraphQLValidatable
    {
        public Guid JokeId { get; set; }

        public ReactionType Type { get; set; }

        public bool IsValid(IServiceProvider serviceProvider)
        {
            return true;
        }
    }
}
