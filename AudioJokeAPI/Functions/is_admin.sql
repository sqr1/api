-- Returns whether the user is an admin
-- Raises if user with specified user id does not exist
CREATE OR REPLACE FUNCTION is_admin(_user_id uuid) RETURNS bool
    LANGUAGE plpgsql
AS
$$
DECLARE
    is_user_admin_ bool;
BEGIN
    SELECT is_admin INTO is_user_admin_ FROM users WHERE id = _user_id;
    IF is_user_admin_ IS NULL
    THEN
        RAISE EXCEPTION 'IS_ADMIN_001' USING HINT = 'User does not exist';
    END IF;

    RETURN is_user_admin_;
END;
$$;
