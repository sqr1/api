﻿using GraphQL;
using System.Collections.Generic;
using AudioJokeAPI.Services;

namespace AudioJokeAPI.ErrorHandling
{
    public static class GraphQLErrorHandler
    {
        public const string ERROR_VALIDATION = "VALIDATION";

        public const string ERROR_UNKNOWN = "UNKNOWN";

        public const string ERROR_UNAUTHORIZED = "INVALID_TOKEN";

        public const string ERROR_FORBIDDEN = "FORBIDDEN";

        public const string ERROR_TOO_MANY_ATTEMPTS = "TOO_MANY_ATTEMPTS";

        public const string ERROR_TIMEOUT = "TIMEOUT";

        public const string POSTGRES_UNAUTHORIZED_EXCEPTION = "GLOBAL_INVALID_TOKEN";

        public const string POSTGRES_TOO_MANY_ATTEMPTS_EXCEPTION = "GLOBAL_TOO_MANY_ATTEMPTS";

        public const string POSTGRES_TIMEOUT_EXCEPTION = "GLOBAL_TIMEOUT";

        public static ExecutionError NewValidationError()
        {
            return new ExecutionError(ERROR_VALIDATION,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 400)
                    }
                )
            );
        }

        public static ExecutionError NewUnathorizedError()
        {
            return new ExecutionError(ERROR_UNAUTHORIZED,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 401)
                    }
                )
            );
        }

        public static ExecutionError NewForbiddenError()
        {
            return new ExecutionError(ERROR_FORBIDDEN,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 403)
                    }
                )
            );
        }

        public static ExecutionError NewUnknownError()
        {
            return new ExecutionError(ERROR_UNKNOWN,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 500)
                    }
                )
            );
        }

        public static ExecutionError NewTooManyAttemptsError()
        {
            return new ExecutionError(ERROR_TOO_MANY_ATTEMPTS,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 429)
                    }
                )
            );
        }

        public static ExecutionError NewTimeoutError()
        {
            return new ExecutionError(ERROR_TIMEOUT,
                new Dictionary<string, object>
                (
                    new[] {
                        new KeyValuePair<string, object>("code", 408)
                    }
                )
            );
        }

        public static void RemoveAccessTokenCookieIfUnathorized(Npgsql.PostgresException exception, IClientService clientService)
        {
            if (exception.MessageText == POSTGRES_UNAUTHORIZED_EXCEPTION)
            {
                clientService.DeleteAccessToken();
            }
        }

        public static ExecutionError FromPostgresException(Npgsql.PostgresException exception)
        {
            if (exception.SqlState != "P0001")
            {
                return NewUnknownError();
            }

            if (exception.MessageText == POSTGRES_UNAUTHORIZED_EXCEPTION)
            {
                return NewUnathorizedError();
            }

            if (exception.MessageText == POSTGRES_TOO_MANY_ATTEMPTS_EXCEPTION)
            {
                return NewTooManyAttemptsError();
            }

            if (exception.MessageText == POSTGRES_TIMEOUT_EXCEPTION)
            {
                return NewTimeoutError();
            }

            return NewForbiddenError();
        }
    }
}
