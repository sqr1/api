﻿using System;
using AudioJokeAPI.Models;

namespace AudioJokeAPITest.Models
{
    public class GraphQLMutationResult
    {
        public Guid? RequestCode { get; set; }

        public Session SignInViaGoogle { get; set; }

        public Session RefreshSession { get; set; }

        public string Logout { get; set; }

        public User RegisterUser { get; set; }

        public string EditUser { get; set; }

        public string DeleteUser { get; set; }

        public Joke CreateJoke { get; set; }

        public string DeleteJoke { get; set; }

        public Reaction CreateReaction { get; set; }

        public string DeleteReaction { get; set; }
    }
}
