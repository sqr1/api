﻿using GraphQL.Types;
using AudioJokeAPI.Models.Inputs;

namespace AudioJokeAPI.Schema.Inputs
{
    public class NewPagedArrayObject : InputObjectGraphType<NewPagedArray>
    {
        public NewPagedArrayObject()
        {
            Name = "NewPageArray";
            Field("page", x => x.Page);
            Field("perPage", x => x.PerPage);
        }
    }
}
