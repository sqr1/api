﻿using AudioJokeAPI.Schema;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AudioJokeAPI.Models.Inputs
{
    public class NewUser : IGraphQLValidatable
    {
        public string Nickname { get; set; }

        public bool IsValid(IServiceProvider provider)
        {
            return !string.IsNullOrWhiteSpace(Nickname) && 
                   Regex.IsMatch(Nickname, @"^[a-z0-9\.\-_]+$");
        }
    }
}
