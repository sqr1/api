﻿using System;

namespace AudioJokeAPI.Schema
{
    public interface IGraphQLValidatable
    {
        public bool IsValid(IServiceProvider provider);
    }
}
