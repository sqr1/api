﻿using AudioJokeAPI.DataAccess;
using AudioJokeAPI.Models;
using Microsoft.AspNetCore.Http;
using System;

namespace AudioJokeAPI.Services
{
    public class ClientService : IClientService
    {
        private readonly IHttpContextAccessor _accessor;

        private readonly User _user;

        public ClientService(IHttpContextAccessor accessor, IRepository repository)
        {
            this._accessor = accessor;

            if (GetAccessToken() == null)
            {
                return;
            }

            _user = repository.GetUserByAccessToken(GetAccessToken());
        }

        public string GetHost()
        {
            return _accessor.HttpContext.Request.Host.Host;
        }

        public string GetAccessToken()
        {
            return _accessor.HttpContext.Request.Cookies["access_token"];
        }

        public void SetAccessToken(string token)
        {
            _accessor.HttpContext.Response.Cookies.Append("access_token", token, new CookieOptions()
            {
                IsEssential = true,
                Expires = DateTime.Now.AddMonths(3)
            });
        }

        public void DeleteAccessToken()
        {
            _accessor.HttpContext.Response.Cookies.Delete("access_token");
        }

        public Guid GetUserId()
        {
            if (_user == null)
            {
                return Guid.Empty;
            }
            return _user.Id;
        }

        public bool IsAdmin()
        {
            if (_user == null)
                return false;
            return _user.IsAdmin;
        }

        public bool IsRegistered()
        {
            if (_user == null)
                return false;
            return _user.IsRegistered;
        }
    }
}
