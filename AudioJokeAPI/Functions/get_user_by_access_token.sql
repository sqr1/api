﻿CREATE OR REPLACE FUNCTION get_user_by_access_token(_access_token varchar)
    RETURNS SETOF users
    LANGUAGE plpgsql
AS
$$
DECLARE
    user_id_ uuid := (SELECT user_id FROM sessions WHERE access_token = _access_token LIMIT 1);
BEGIN
    RETURN QUERY (
        SELECT * FROM users WHERE id = user_id_
    );
END;
$$;
