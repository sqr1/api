﻿using GraphQL;
using GraphQL.Types;
using AudioJokeAPI.DataAccess;
using AudioJokeAPI.ErrorHandling;
using AudioJokeAPI.Models;
using AudioJokeAPI.Models.Inputs;
using AudioJokeAPI.Schema.Inputs;
using AudioJokeAPI.Schema.Types;
using AudioJokeAPI.Services;
using System;

namespace AudioJokeAPI.Schema
{
    public class MutationObject : ObjectGraphType<object>
    {
        private readonly ISchemaValidation _schemaValidation;

        public MutationObject(IClientService clientService,
                              IRepository repository,
                              ISchemaValidation schemaValidation,
                              ICDNService cdnService,
                              IGoogleAuthenticationService googleAuthenticationService)
        {
            _schemaValidation = schemaValidation;

            Name = "Mutation";

            Field<SessionObject>(
                "mockSignIn",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>>
                    {
                        Name = "email"
                    }),
                resolve: context =>
                {
                    var email = context.GetArgument<string>("email");

                    Session session;
                    try
                    {
                        session = repository.CreateSession(email, clientService.GetHost());
                    }
                    catch (Npgsql.PostgresException)
                    {
                        context.Errors.Add(GraphQLErrorHandler.NewUnknownError());
                        return null;
                    }
                    clientService.SetAccessToken(session.AccessToken);
                    return session;
                }
            );

            Field<SessionObject>(
                "signInViaGoogle",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>>
                    {
                        Name = "token"
                    }),
                resolve: context =>
                {
                    if (!schemaValidation.GetTokenArgument(context, "token", out var token))
                    {
                        return null;
                    }

                    if (!googleAuthenticationService.Verify(token, out var result))
                    {
                        context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                        return null;
                    }
                    Session session;
                    try
                    {
                        session = repository.CreateSession(result.Email, clientService.GetHost());
                    }
                    catch (Npgsql.PostgresException)
                    {
                        context.Errors.Add(GraphQLErrorHandler.NewUnknownError());
                        return null;
                    }
                    clientService.SetAccessToken(session.AccessToken);
                    return session;
                }
            );

            Field<SessionObject>(
                "refreshSession",
                resolve: context =>
                {
                    Session session;

                    try
                    {
                        session = repository.Refresh(clientService.GetAccessToken());
                    }
                    catch (Npgsql.PostgresException ex)
                    {
                        GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientService);
                        context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                        return null;
                    }

                    if (session != null)
                    {
                        clientService.SetAccessToken(session.AccessToken);
                    }

                    return session;
                }
            );

            Field<StringGraphType>(
                "logout",
                resolve: context =>
                {
                    try
                    {
                        repository.Logout(clientService.GetAccessToken());
                    }
                    catch (Npgsql.PostgresException ex)
                    {
                        GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientService);
                        context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                        return null;
                    }

                    clientService.DeleteAccessToken();

                    return "success";
                }
            );

            FieldForCreating<UserObject, NewUserObject, NewUser, User>(
                "registerUser", "user", clientService, entity =>
                repository.RegisterUser(clientService.GetAccessToken(), entity.Nickname));
            FieldForEditing<EditedUserObject, EditedUser>("editUser", "user", clientService, entity =>
               repository.EditUser(clientService.GetAccessToken(), entity.Id, entity.Nickname, entity.Bio));
            FieldWithSingleIdArgument("deleteUser", "userId", clientService, (id) => repository.DeleteUser(clientService.GetAccessToken(), id));

            FieldForCreating<JokeObject, NewJokeObject, NewJoke, Joke>(
                "createJoke", "joke", clientService,
                entity =>
                {
                    var pollyService = new PollyService("AKIAI76DNVJAUXRGJRZA", "Qz9xsKEyytXe6DXwHe+MU+QW9Eb0fmVP1Olkaylw", Amazon.RegionEndpoint.EUCentral1);
                    var jokeAudio = pollyService.TextToSpeech(entity.Text);

                    var joke = repository.CreateJoke(clientService.GetAccessToken(), entity.Text);
                    if (cdnService.UploadAudioToCDN(jokeAudio, joke.Id)) return joke;
                    repository.DeleteJoke(joke.Id);
                    throw new Exception("Cannot upload the audio on CDN.");
                });
            FieldWithSingleIdArgument("deleteJoke", "jokeId", clientService,
                id =>
                {
                    repository.DeleteJoke(clientService.GetAccessToken(), id);
                    cdnService.DeleteAudioFromCDN(id);
                });

            FieldForCreating<ReactionObject, NewReactionObject, NewReaction, Reaction>(
                "createReaction", "reaction", clientService, entity =>
                repository.CreateReaction(clientService.GetAccessToken(), entity.JokeId, entity.Type));
            FieldWithSingleIdArgument("deleteReaction", "reactionId", clientService, (id) => repository.DeleteReaction(clientService.GetAccessToken(), id));
        }

        private FieldType FieldForCreating<TEntityObject, TNewEntityObject, TNewEntity, TEntity>(string name,
            string argumentName,
            IClientService clientService,
            Func<TNewEntity, TEntity> func)
            where TEntityObject : ObjectGraphType<TEntity>
            where TNewEntityObject : InputObjectGraphType<TNewEntity>
            where TNewEntity : IGraphQLValidatable
        {
            return Field<TEntityObject>(
                 name,
                 arguments: new QueryArguments(
                     new QueryArgument<NonNullGraphType<TNewEntityObject>>
                     {
                         Name = argumentName
                     }),
                 resolve: context =>
                 {
                     if (!_schemaValidation.GetValidatableArgument(context, argumentName, out TNewEntity entity))
                         return null;
                     TEntity result;
                     try
                     {
                         result = func(entity);
                     }
                     catch (Npgsql.PostgresException ex)
                     {
                         GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientService);
                         context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                         return null;
                     }
                     catch (Exception)
                     {
                         context.Errors.Add(GraphQLErrorHandler.NewUnknownError());
                         return null;
                     }
                     return result;
                 }
            );
        }

        private FieldType FieldForEditing<TEntityObject, TEntity>(string name, string argumentName, IClientService clientService, Action<TEntity> action)
            where TEntityObject : GraphType
            where TEntity : IGraphQLValidatable
        {
            return Field<StringGraphType>(
                name,
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<TEntityObject>>
                    {
                        Name = argumentName
                    }),
                resolve: context =>
                {
                    if (!_schemaValidation.GetValidatableArgument(context, argumentName, out TEntity entity))
                        return null;
                    try
                    {
                        action(entity);
                    }
                    catch (Npgsql.PostgresException ex)
                    {
                        GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientService);
                        context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                        return null;
                    }
                    catch (Exception)
                    {
                        context.Errors.Add(GraphQLErrorHandler.NewUnknownError());
                        return null;
                    }
                    return "success";
                }
            );
        }

        private FieldType FieldWithSingleIdArgument(string name, string argumentName, IClientService clientService, Action<Guid> action)
        {
            return Field<StringGraphType>(
              name,
              arguments: new QueryArguments(
                  new QueryArgument<NonNullGraphType<GuidGraphType>>
                  {
                      Name = argumentName
                  }),
              resolve: context =>
              {
                  var id = context.GetArgument<Guid>(argumentName);
                  try
                  {
                      action(id);
                  }
                  catch (Npgsql.PostgresException ex)
                  {
                      GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientService);
                      context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                      return null;
                  }
                  return "success";
              }
          );
        }
    }
}
