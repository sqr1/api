CREATE OR REPLACE FUNCTION delete_user(_token varchar, _id uuid) RETURNS BOOLEAN AS
$$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    user_    Users%ROWTYPE;
BEGIN
    IF session_.user_id <> _id
    THEN
        IF (SELECT is_admin FROM is_admin(session_.user_id) LIMIT 1)
        THEN
            -- user tries to modify another user while having no permission to do that
            RAISE EXCEPTION 'DELETE_USER_001' USING HINT = 'Forbidden';
        END IF;
    END IF;

    SELECT * INTO user_ from users where id = _id LIMIT 1;
    IF user_ IS NULL THEN
        -- user with specified id does not exist (happens when admin tries to delete non-existent user)
        RAISE EXCEPTION 'DELETE_USER_002' USING HINT = 'Invalid user id';
    END IF;

    IF NOT user_.is_registered
    THEN
        RAISE EXCEPTION 'DELETE_USER_003' USING HINT = 'Unregistered user';
    END IF;

    DELETE FROM users WHERE id = user_.id;

    RETURN True;
END;
$$ LANGUAGE plpgsql;
