﻿using GraphQL.Types;
using AudioJokeAPI.Models.Inputs;

namespace AudioJokeAPI.Schema.Inputs
{
    public sealed class EditedUserObject : InputObjectGraphType<EditedUser>
    {
        public EditedUserObject()
        {
            Name = "EditedUser";
            Field("id", x => x.Id);
            Field("nickname", x => x.Nickname, nullable: true);
            Field("bio", x => x.Bio, nullable: true);
        }
    }
}
