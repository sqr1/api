﻿using GraphQL.Types;
using AudioJokeAPI.Models;

namespace AudioJokeAPI.Schema.Types
{
    public class PagedArrayObject<N, M> : ObjectGraphType<PagedArray<M>> where N : ObjectGraphType<M>
    {
        public PagedArrayObject()
        {
            Name = typeof(M).Name + "PageArray";
            Field("totalAmount", x => x.TotalAmount);
            Field("totalPages", x => x.TotalPages);
            Field<ListGraphType<NonNullGraphType<N>>>("data");
        }
    }
}
