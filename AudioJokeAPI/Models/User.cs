﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AudioJokeAPI.Models
{
    [Table("users")]
    public partial class User : BaseModel<User>
    {
        public User()
        {
            Jokes = new HashSet<Joke>();
            Reactions = new HashSet<Reaction>();
            Sessions = new HashSet<Session>();
        }

        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsRegistered { get; set; }
        public string Nickname { get; set; }
        public string Bio { get; set; }

        public virtual ICollection<Joke> Jokes { get; set; }
        public virtual ICollection<Reaction> Reactions { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }
    }
}
