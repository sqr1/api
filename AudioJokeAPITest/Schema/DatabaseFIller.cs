﻿using AudioJokeAPI.DataAccess;
using AudioJokeAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using AudioJokeAPITest.Helpers;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;


namespace AudioJokeAPITest.Schema
{
    public class DatabaseFiller
    {
        private const int ImagesCount = 3326;

        private const int UsersCount = 100;

        private const int ReactionsPerUser = 100;

        private const int LatentSpaceSize = 64;

        private const int TextClustersCount = 10;

        private static Random _rand = new Random();

        private IConfiguration _configuration;

        [SetUp]
        public void SetUp()
        {
            _configuration = ConfigurationManager.Build();
        }

        private void SetUp(PostgresContext context)
        {
            var connection = context.Database.GetDbConnection();
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
            using var command = connection.CreateCommand();
            command.CommandText = "SELECT tablename FROM pg_tables WHERE schemaname = 'public' AND tablename <> 'migrations'";
            command.Prepare();

            var tablesToDelete = new List<String>();
            using (var result = command.ExecuteReader())
            {
                while (result.Read())
                {
                    tablesToDelete.Add(result.GetString(0));
                }
            }
            connection.Close();

            context.Database.ExecuteSqlRaw($"TRUNCATE { String.Join(", ", tablesToDelete.ToArray()) } RESTART IDENTITY CASCADE");

        }

        [Test]
        public void Fill()
        {
            var services = new ServiceCollection();
            services.AddDbContextPool<PostgresContext>(options => options.UseNpgsql(_configuration.GetConnectionString("Database")));
            var context = services.BuildServiceProvider().GetRequiredService<PostgresContext>();

            SetUp(context);

            var userIds = new List<Guid>();

            for (var i = 0; i < UsersCount; i++)
            {
                var id = Guid.NewGuid();
                context.Users.Add(new User
                {
                    Id = id,
                    CreatedAt = DateTime.Now,
                    Email = $"user_{i}@gmail.com",
                    IsAdmin = false,
                    IsRegistered = true,
                    Nickname = "user_" + i,
                    Bio = "bio"
                });
                userIds.Add(id);
            }

            var postIds = new List<Guid>();

            for (var i = 0; i < ImagesCount; i++)
            {
                var id = Guid.NewGuid();
                context.Jokes.Add(new Joke
                {
                    Id = id,
                    CreatedAt = DateTime.Now,
                    UserId = userIds[i % userIds.Count],
                    Text = "joke_text_" + i,
                });
                postIds.Add(id);
            }

            foreach (var userId in userIds)
            {
                foreach (var jokeId in PickRandom(postIds.ToArray(), ReactionsPerUser))
                {
                    context.Reactions.Add(new Reaction
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.Now,
                        UserId = userId,
                        JokeId = jokeId,
                        Type = GetRandomReactionType()
                    });
                }
            }

            context.SaveChanges();
        }

        private static List<T> PickRandom<T>(T[] values, int numValues)
        {
            // Create the Random object if it doesn't exist.
            if (_rand == null) _rand = new Random();

            // Don't exceed the array's length.
            if (numValues >= values.Length)
                numValues = values.Length - 1;

            // Make an array of indexes 0 through values.Length - 1.
            int[] indexes =
                Enumerable.Range(0, values.Length).ToArray();

            // Build the return list.
            List<T> results = new List<T>();

            // Randomize the first num_values indexes.
            for (int i = 0; i < numValues; i++)
            {
                // Pick a random entry between i and values.Length - 1.
                int j = _rand.Next(i, values.Length);

                // Swap the values.
                int temp = indexes[i];
                indexes[i] = indexes[j];
                indexes[j] = temp;

                // Save the ith value.
                results.Add(values[indexes[i]]);
            }

            // Return the selected items.
            return results;
        }

        private static ReactionType GetRandomReactionType()
        {
            var r = _rand.Next(4);
            return r switch
            {
                0 => ReactionType.like,
                1 => ReactionType.none,
                2 => ReactionType.dislike,
                3 => ReactionType.report,
                _ => ReactionType.none
            };
        }

        private static double[] GenerateRandomDoubleVector(int length)
        {
            var vector = new double[length];
            for (var i = 0; i < vector.Length; i++)
            {
                vector[i] = _rand.NextDouble();
            }
            return vector;
        }
    }
}
