﻿//using AudioJokeAPI.DataAccess;
//using AudioJokeAPI.Models;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.DependencyInjection;
//using Npgsql;
//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.IO;

//namespace AudioJokeAPITest.DataAccess
//{
//    [TestFixture]
//    public class RepositoryTest
//    {
//        private class PredefinedUser
//        {
//            public readonly Guid UserId;
//            public readonly DateTimeOffset CreatedAt;
//            public readonly string PhoneNumber;
//            public readonly bool IsAdmin;
//            public readonly bool IsRegistered;
//            public readonly int Attempts;
//            public readonly DateTimeOffset LastAttemptedAt;
//            public readonly string Nickname;
//            public readonly string Bio;
//            public readonly string Image;
//            public readonly Guid SessionId;
//            public readonly DateTimeOffset SessionCreatedAt;
//            public readonly string VerificationCode;
//            public readonly string IpAddress;
//            public readonly string AccessToken;
//            public readonly DateTimeOffset AccessTokenExpiryDate;
//            public PredefinedUser(Guid userId, DateTimeOffset createdAt, string phoneNumber, bool isAdmin, bool isRegistered,
//                int attempts, DateTimeOffset lastAttemptedAt, string nickname, string bio, string photo,
//                Guid sessionId, DateTimeOffset  sessionCreatedAt, string verificationCode, string ipAddress,
//                string accessToken, DateTimeOffset accessTokenExpiryDate)
//            {
//                UserId = userId;
//                CreatedAt = createdAt;
//                PhoneNumber = phoneNumber;
//                IsAdmin = isAdmin;
//                IsRegistered = isRegistered;
//                Attempts = attempts;
//                LastAttemptedAt = lastAttemptedAt;
//                Nickname = nickname;
//                Bio = bio;
//                Image = photo;
//                SessionId = sessionId;
//                SessionCreatedAt = sessionCreatedAt;
//                VerificationCode = verificationCode;
//                IpAddress = ipAddress;
//                AccessToken = accessToken;
//                AccessTokenExpiryDate = accessTokenExpiryDate;
//            }
//        }

//        private readonly TimeSpan VERIFICATION_DURATION = TimeSpan.FromSeconds(120);
//        private const int ACCESS_TOKEN_DURATION_MONTHS = 3;
//        private const int MAX_ATTEMPTS = 4;

//        private readonly Guid ADMIN_ID = Guid.Parse("00000000-0000-0000-0000-000000000000");
//        private readonly DateTimeOffset ADMIN_CREATED_AT = DateTimeOffset.Parse("11/21/2020 09:25:07 PM +03:00");
//        private const string ADMIN_PHONE_NUMBER = "+00000000000";
//        private const string ADMIN_NICKNAME = "admin";
//        private readonly Guid ADMIN_SESSION_ID = Guid.Parse("00000000-0000-0000-0000-000000000000");
//        private const string ADMIN_IP_ADDRESS = "0.0.0.0";
//        private const string ADMIN_ACCESS_TOKEN = "00000000-0000-0000-0000-000000000000";
//        private readonly DateTimeOffset ADMIN_ACCESS_TOKEN_EXPIRY_DATE = DateTimeOffset.Parse("11/21/2120 09:25:07 PM +03:00");

//        private const string VERIFICATION_CODE = "123456";

//        private readonly Guid PREDEFINED_USER_ID_1 = Guid.Parse("99999999-0000-0000-0000-111111111111");
//        private readonly Guid PREDEFINED_USER_ID_2 = Guid.Parse("99999999-0000-0000-0000-222222222222");
//        private readonly Guid PREDEFINED_USER_ID_3 = Guid.Parse("99999999-0000-0000-0000-333333333333");
//        private readonly Guid PREDEFINED_USER_ID_4 = Guid.Parse("99999999-0000-0000-0000-444444444444");


//        private readonly DateTimeOffset PREDEFINED_CREATED_AT_1 = DateTimeOffset.Parse("11/21/2020 10:25:07 PM +03:00").ToUniversalTime();
//        private readonly DateTimeOffset PREDEFINED_CREATED_AT_2 = DateTimeOffset.Parse("11/21/2020 11:25:07 PM +03:00").ToUniversalTime();

//        private const string PREDEFINED_PHONE_NUMBER_1 = "+79990000001";
//        private const string PREDEFINED_PHONE_NUMBER_2 = "+79990000002";
//        private const string PREDEFINED_PHONE_NUMBER_3 = "+79990000003";

//        private const string PREDEFINED_NICKNAME_1 = "PREDEFINED_user_1";
//        private const string PREDEFINED_NICKNAME_2 = "PREDEFINED_user_2";
//        private const string PREDEFINED_NICKNAME_3 = "PREDEFINED_user_3";

//        private readonly Guid PREDEFINED_SESSION_ID_1 = Guid.Parse("88888888-0000-0000-0000-111111111111");
//        private readonly Guid PREDEFINED_SESSION_ID_2 = Guid.Parse("88888888-0000-0000-0000-222222222222");
//        private readonly Guid PREDEFINED_SESSION_ID_3 = Guid.Parse("88888888-0000-0000-0000-333333333333");
//        private readonly Guid PREDEFINED_SESSION_ID_4 = Guid.Parse("88888888-0000-0000-0000-444444444444");

//        private const string PREDEFINED_IP_ADDRESS_1 = "99.0.0.1";
//        private const string PREDEFINED_IP_ADDRESS_2 = "99.0.0.2";
//        private const string PREDEFINED_IP_ADDRESS_3 = "99.0.0.3";
//        private const string PREDEFINED_IP_ADDRESS_4 = "99.0.0.4";

//        private const string PREDEFINED_ACCESS_TOKEN_1 = "11111111-0000-0000-0000-111111111111";
//        private const string PREDEFINED_ACCESS_TOKEN_2 = "11111111-0000-0000-0000-222222222222";
//        private const string PREDEFINED_ACCESS_TOKEN_3 = "11111111-0000-0000-0000-333333333333";
//        private const string PREDEFINED_ACCESS_TOKEN_4 = "11111111-0000-0000-0000-444444444444";

//        private const string PHONE_NUMBER_1 = "+71111111111";
//        private const string IP_ADDRESS_1 = "100.0.0.1";
//        private const string NICKNAME_1 = "nickname_1";

//        private const string PHONE_NUMBER_2 = "+71112222222";
//        private const string IP_ADDRESS_2 = "100.0.0.2";
//        private const string NICKNAME_2 = "nickname_2";

//        private IRepository repository;
//        private PostgresContext context;

//        private NpgsqlParameter CreateNpgsqlParameter(string key, object value)
//        {
//            return new NpgsqlParameter(key, value == null ? DBNull.Value : value);
//        }

//        private DateTimeOffset TruncateDateTimeOffsetToSeconds(DateTimeOffset dt)
//        {
//            return dt.AddTicks(-(dt.Ticks % TimeSpan.TicksPerSecond));
//        }

//        private bool AreDateTimeOffsetsEqualWithError(DateTimeOffset dt1, DateTimeOffset dt2, TimeSpan eps)
//        {
//            var diff = DateTimeOffset.Compare(dt1, dt2) > 0 ? dt1.Subtract(dt2) : dt2.Subtract(dt1);
//            return !(TimeSpan.Compare(diff, eps) == 1);
//        }

//        private void AddPredefinedUser(PredefinedUser predefinedUser)
//        {
//            context.Database.ExecuteSqlRaw(
//                "INSERT INTO users (id, created_at, phone_number, is_admin, is_registered, attempts, last_attempted_at, nickname, bio, photo) " +
//                "VALUES (@userId, @createdAt, @phoneNumber, @isAdmin, @isRegistered, @attempts, @lastAttemptedAt, @nickname, @bio, @photo)",
//                CreateNpgsqlParameter("@userId", predefinedUser.UserId),
//                CreateNpgsqlParameter("@createdAt", predefinedUser.CreatedAt),
//                CreateNpgsqlParameter("@phoneNumber", predefinedUser.PhoneNumber),
//                CreateNpgsqlParameter("@isAdmin", predefinedUser.IsAdmin),
//                CreateNpgsqlParameter("@isRegistered", predefinedUser.IsRegistered),
//                CreateNpgsqlParameter("@attempts", predefinedUser.Attempts),
//                CreateNpgsqlParameter("@lastAttemptedAt", predefinedUser.LastAttemptedAt),
//                CreateNpgsqlParameter("@nickname", predefinedUser.Nickname),
//                CreateNpgsqlParameter("@bio", predefinedUser.Bio),
//                CreateNpgsqlParameter("@photo", predefinedUser.Image));

//            context.Database.ExecuteSqlRaw(
//                "INSERT INTO sessions (id, created_at, user_id, verification_code, ip, access_token, access_token_expiry_date) " +
//                "VALUES (@sessionId, @createdAt, @userId, @verificationCode, @ipAddress, @accessToken, @accessTokenExpiryDate)",
//                CreateNpgsqlParameter("@sessionId", predefinedUser.SessionId),
//                CreateNpgsqlParameter("@createdAt", predefinedUser.SessionCreatedAt),
//                CreateNpgsqlParameter("@userId", predefinedUser.UserId),
//                CreateNpgsqlParameter("@verificationCode", predefinedUser.VerificationCode),
//                CreateNpgsqlParameter("@ipAddress", predefinedUser.IpAddress),
//                CreateNpgsqlParameter("@accessToken", predefinedUser.AccessToken),
//                CreateNpgsqlParameter("@accessTokenExpiryDate", predefinedUser.AccessTokenExpiryDate));
//        }

//        [OneTimeSetUp]
//        public void Init()
//        {
//            var services = new ServiceCollection();
//            services.AddTransient<IRepository, Repository>();
//            services.AddDbContextPool<PostgresContext>(options => options.UseNpgsql(Migrations.GetConnectionString()));

//            context = services.BuildServiceProvider().GetRequiredService<PostgresContext>();
//            repository = services.BuildServiceProvider().GetRequiredService<IRepository>();
//        }

//        [SetUp]
//        public void SetUp()
//        {
//            var connection = context.Database.GetDbConnection();
//            if (connection.State != System.Data.ConnectionState.Open)
//            {
//                connection.Open();
//            }
//            using var command = connection.CreateCommand();
//            command.CommandText = "SELECT tablename FROM pg_tables WHERE schemaname = 'public' AND tablename <> 'migrations'";
//            command.Prepare();

//            var tablesToDelete = new List<String>();
//            using (var result = command.ExecuteReader())
//            {
//                while (result.Read())
//                {
//                    tablesToDelete.Add(result.GetString(0));
//                }
//            }
//            connection.Close();

//            context.Database.ExecuteSqlRaw($"TRUNCATE { String.Join(", ", tablesToDelete.ToArray()) } RESTART IDENTITY CASCADE");

//            // Adds predefined admin
//            AddPredefinedUser(new PredefinedUser(
//                ADMIN_ID, ADMIN_CREATED_AT, ADMIN_PHONE_NUMBER, true, true, 0, ADMIN_CREATED_AT, ADMIN_NICKNAME, "", null,
//                ADMIN_SESSION_ID, ADMIN_CREATED_AT, "", ADMIN_IP_ADDRESS, ADMIN_ACCESS_TOKEN,
//                ADMIN_ACCESS_TOKEN_EXPIRY_DATE));
//        }

//        [Test]
//        public void TestRequestCodeFirstTimeSuccessful()
//        {
//            var sessionId = repository.RequestCode(PHONE_NUMBER_1, VERIFICATION_CODE, IP_ADDRESS_1);

//            var session = context.Sessions.Find(sessionId);
//            Assert.IsNotNull(session, "Session has not been created");

//            Assert.AreEqual(VERIFICATION_CODE, session.VerificationCode, "Invalid verification code");
//            Assert.AreEqual(IP_ADDRESS_1, session.Ip, "Invalid ip address");
//            Assert.AreEqual(session.CreatedAt.AddMonths(ACCESS_TOKEN_DURATION_MONTHS), session.AccessTokenExpiryDate,
//                $"AccessTokenExpireDate should be { ACCESS_TOKEN_DURATION_MONTHS } months later than session creation time");

//            // Validates that new user has been created
//            var user = context.Users.Find(session.UserId);
//            Assert.IsNotNull(user, "User has not been created");

//            Assert.AreEqual(PHONE_NUMBER_1, user.PhoneNumber, "Invalid phone number");
//            Assert.IsFalse(user.IsAdmin, "User should not be an admin");
//            Assert.IsFalse(user.IsRegistered, "User should not be registered");
//            Assert.Zero(user.Attempts, "Initial number of attemps should be zero");
//            var a = DateTimeOffset.Now.AddHours(-3).ToString();
//            Assert.AreEqual(user.CreatedAt, user.LastAttemptedAt, "Initial value of LastAttemptedAt field should equals user creation time");
//            Assert.That(Guid.TryParse(user.Nickname, out _), "Default nickname should be a guid");
//            Assert.IsEmpty(user.Bio, "Initial bio should be empty");
//            Assert.IsNull(user.Image, "Initial photo should be null");

//            Assert.AreEqual(session.CreatedAt, user.CreatedAt, "At first request user and session should have the same creation time");
//        }

//        [Test]
//        public void TestRequestCodeRegisteredUserSuccessful()
//        {
//            AddPredefinedUser(new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, PREDEFINED_CREATED_AT_1, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1, PREDEFINED_ACCESS_TOKEN_1,
//                PREDEFINED_CREATED_AT_1.AddMonths(ACCESS_TOKEN_DURATION_MONTHS)));

//            var sessionId = repository.RequestCode(PREDEFINED_PHONE_NUMBER_1, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_2);

//            var session = context.Sessions.Find(sessionId);
//            Assert.IsNotNull(session, "Session has not been created");

//            Assert.AreEqual(VERIFICATION_CODE, session.VerificationCode, "Invalid verification code");
//            Assert.AreEqual(PREDEFINED_IP_ADDRESS_2, session.Ip, "Invalid ip address");
//            Assert.AreEqual(session.CreatedAt.AddMonths(ACCESS_TOKEN_DURATION_MONTHS), session.AccessTokenExpiryDate,
//                $"AccessTokenExpireDate should be { ACCESS_TOKEN_DURATION_MONTHS } months later than session creation time");
//            Assert.AreEqual(PREDEFINED_USER_ID_1, session.UserId, "Invalid user id");
//        }

//        [Test]
//        public void TestVerifyCodeUnregisteredUserSuccessful()
//        {
//            var initialUserNickname = Guid.NewGuid().ToString();
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, false, MAX_ATTEMPTS-1, PREDEFINED_CREATED_AT_1, initialUserNickname, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1, PREDEFINED_ACCESS_TOKEN_1,
//                PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            var session = repository.VerifyCode(predefinedUser.SessionId, predefinedUser.VerificationCode);
//            Assert.IsNotNull(session, "Returned session should not be null");

//            Assert.AreEqual(predefinedUser.SessionId, session.Id, "Invalid session id");
//            Assert.IsTrue(AreDateTimeOffsetsEqualWithError(predefinedUser.SessionCreatedAt, session.CreatedAt, TimeSpan.FromMilliseconds(10)),
//                "Session creation time has been changed.\n    Expected: {0}\n    Got: {1}", predefinedUser.SessionCreatedAt, session.CreatedAt);
//            Assert.AreEqual(predefinedUser.UserId, session.UserId, "User id has been changed");
//            Assert.AreEqual(predefinedUser.VerificationCode, session.VerificationCode, "Verification code has been changed");
//            Assert.AreEqual(predefinedUser.IpAddress, session.Ip, "Ip address has been changed");
//            Assert.AreEqual(predefinedUser.AccessToken, session.AccessToken, "Access token has been changed");
//            Assert.IsTrue(AreDateTimeOffsetsEqualWithError(predefinedUser.AccessTokenExpiryDate, session.AccessTokenExpiryDate, TimeSpan.FromMilliseconds(10)),
//                "Access token expiry date has been changed.\n    Expected: {0}\n    Got: {1}", predefinedUser.AccessTokenExpiryDate, session.AccessTokenExpiryDate);

//            var user = context.Users.Find(session.UserId);
//            Assert.Zero(user.Attempts, "Attempts has not been reset to zero");
//        }

//        [Test]
//        public void TestVerifyCodeRegisteredUserSuccessful()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, MAX_ATTEMPTS, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1, PREDEFINED_ACCESS_TOKEN_1,
//                PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            var session = repository.VerifyCode(predefinedUser.SessionId, predefinedUser.VerificationCode);
//            Assert.IsNotNull(session, "Returned session should not be null");

//            Assert.AreEqual(predefinedUser.SessionId, session.Id, "Invalid session id");
//            Assert.IsTrue(AreDateTimeOffsetsEqualWithError(predefinedUser.SessionCreatedAt, session.CreatedAt, TimeSpan.FromMilliseconds(10)),
//                "Session creation time has been changed.\n    Expected: {0}\n    Got: {1}", predefinedUser.SessionCreatedAt, session.CreatedAt);
//            Assert.AreEqual(predefinedUser.UserId, session.UserId, "User id has been changed");
//            Assert.AreEqual(predefinedUser.VerificationCode, session.VerificationCode, "Verification code has been changed");
//            Assert.AreEqual(predefinedUser.IpAddress, session.Ip, "Ip address has been changed");
//            Assert.AreEqual(predefinedUser.AccessToken, session.AccessToken, "Access token has been changed");
//            Assert.IsTrue(AreDateTimeOffsetsEqualWithError(predefinedUser.AccessTokenExpiryDate, session.AccessTokenExpiryDate, TimeSpan.FromMilliseconds(10)),
//                "Access token expiry date has been changed.\n    Expected: {0}\n    Got: {1}", predefinedUser.AccessTokenExpiryDate, session.AccessTokenExpiryDate);
//            //Assert.AreEqual(TruncateDateTimeOffsetToSeconds(predefinedUser.AccessTokenExpiryDate), TruncateDateTimeOffsetToSeconds(session.AccessTokenExpiryDate), );

//            var user = context.Users.Find(session.UserId);
//            Assert.Zero(user.Attempts, "Attempts has not been reset to zero");
//        }

//        [Test]
//        public void TestVerifyCodeInvalidSessionIdFail()
//        {
//            var invalidSessionId = Guid.NewGuid();
//            try
//            {
//                var session = repository.VerifyCode(invalidSessionId, VERIFICATION_CODE);
//                Assert.Fail("Exception should be thrown but was not");
//            } catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "VERIFY_CODE_001", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestVerifyCodeMaxAttemptsExcess()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, MAX_ATTEMPTS+1, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1, PREDEFINED_ACCESS_TOKEN_1,
//                PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            try
//            {
//                var session = repository.VerifyCode(PREDEFINED_SESSION_ID_1, VERIFICATION_CODE);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "VERIFY_CODE_002", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestVerifyCodeInvalidVerificationCode()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1, PREDEFINED_ACCESS_TOKEN_1,
//                PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            var invalidVerificationCode = "987654";

//            try
//            {
//                var session = repository.VerifyCode(PREDEFINED_SESSION_ID_1, invalidVerificationCode);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "VERIFY_CODE_003", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestVerifyCodeVerificationDurationExcess()
//        {
//            var sessionCreationTime = DateTimeOffset.Now.Subtract(VERIFICATION_DURATION.Add(TimeSpan.FromSeconds(1)));
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, sessionCreationTime, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1,
//                PREDEFINED_ACCESS_TOKEN_1, PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            try
//            {
//                var session = repository.VerifyCode(PREDEFINED_SESSION_ID_1, VERIFICATION_CODE);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "VERIFY_CODE_004", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestRefreshSuccessful()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1,
//                PREDEFINED_ACCESS_TOKEN_1, PREDEFINED_CREATED_AT_1.AddDays(1));
//            AddPredefinedUser(predefinedUser);

//            var session = repository.Refresh(predefinedUser.AccessToken);
//            Assert.IsNotNull(session, "Returned session should not be null");

//            Assert.IsTrue(AreDateTimeOffsetsEqualWithError(DateTimeOffset.Now.AddMonths(ACCESS_TOKEN_DURATION_MONTHS), session.AccessTokenExpiryDate, TimeSpan.FromSeconds(5)), 
//                "Invalid access token expiry date. \n    Expected: {0}\n    Got {1}", DateTimeOffset.Now.AddMonths(ACCESS_TOKEN_DURATION_MONTHS), session.AccessTokenExpiryDate);
//            Assert.AreNotEqual(predefinedUser.AccessToken, session.AccessToken, "Access token has not been changed");
//        }

//        [Test]
//        public void TestRefreshInvalidAccessToken()
//        {
//            var invalidAccessToken = Guid.NewGuid().ToString();
//            try
//            {
//                var session = repository.Refresh(invalidAccessToken);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "CHECK_SESSION_001", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestRefreshAccessTokenExpired()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, PREDEFINED_CREATED_AT_1, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1,
//                PREDEFINED_ACCESS_TOKEN_1, DateTimeOffset.Now.AddDays(-1));
//            AddPredefinedUser(predefinedUser);

//            try
//            {
//                var session = repository.Refresh(predefinedUser.AccessToken);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "CHECK_SESSION_001", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestLogoutSuccessful() {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, DateTimeOffset.Now, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1,
//                PREDEFINED_ACCESS_TOKEN_1, PREDEFINED_CREATED_AT_1.AddYears(100));
//            AddPredefinedUser(predefinedUser);

//            repository.Logout(predefinedUser.AccessToken);
            
//            var session = context.Sessions.Find(predefinedUser.SessionId);
//            Assert.IsNull(session, "Session has not been deleted");
//        }

//        [Test]
//        public void TestLogoutInvalidAccessToken()
//        {
//            var invalidAccessToken = Guid.NewGuid().ToString();
//            try
//            {
//                repository.Logout(invalidAccessToken);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "CHECK_SESSION_001", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestLogoutAccessTokenExpired()
//        {
//            var predefinedUser = new PredefinedUser(
//                PREDEFINED_USER_ID_1, PREDEFINED_CREATED_AT_1, PREDEFINED_PHONE_NUMBER_1, false, true, 0, PREDEFINED_CREATED_AT_1, PREDEFINED_NICKNAME_1, "", null,
//                PREDEFINED_SESSION_ID_1, PREDEFINED_CREATED_AT_1, VERIFICATION_CODE, PREDEFINED_IP_ADDRESS_1,
//                PREDEFINED_ACCESS_TOKEN_1, DateTimeOffset.Now.AddDays(-1));
//            AddPredefinedUser(predefinedUser);

//            try
//            {
//                repository.Logout(predefinedUser.AccessToken);
//                Assert.Fail("Exception should be thrown but was not");
//            }
//            catch (Npgsql.PostgresException ex)
//            {
//                Assert.AreEqual(ex.SqlState, "P0001", "Invalid excetion error code");
//                Assert.AreEqual(ex.MessageText, "CHECK_SESSION_001", "Invalid exception error message");
//            }
//        }

//        [Test]
//        public void TestRegisterUserSuccessful()
//        {

//        }
//    }
//}
