﻿using GraphQL.Types;
using AudioJokeAPI.ErrorHandling;
using AudioJokeAPI.Models;
using AudioJokeAPI.Services;

namespace AudioJokeAPI.Schema.Types
{
    public class ReactionObject : ObjectGraphType<Reaction>
    {
        public ReactionObject(IDataLoaderService dataLoaderService, IClientService clientService)
        {
            Name = "Reaction";
            Field("id", x => x.Id);
            Field<UserObject>("user",
                resolve: context =>
                {
                    if (clientService.IsAdmin() || clientService.GetUserId() == context.Source.UserId)
                    {
                        return dataLoaderService.GetById<User>(context.Source.UserId);
                    }
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;
                }
            );
            Field<JokeObject>("joke",
                resolve: context =>
                {
                    if (clientService.IsAdmin() || clientService.GetUserId() == context.Source.UserId)
                    {
                        return dataLoaderService.GetById<Joke>(context.Source.JokeId);
                    }
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;
                });
            Field<NonNullGraphType<ReactionTypeObject>>("type");
        }
    }
}
