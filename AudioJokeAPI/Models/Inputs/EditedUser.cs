﻿using AudioJokeAPI.Schema;
using System;
using System.Text.RegularExpressions;

namespace AudioJokeAPI.Models.Inputs
{
    public class EditedUser : IGraphQLValidatable
    {
        public const int BioLength = 70;

        public Guid Id { get; set; }

        public string Nickname { get; set; }

        public string Bio { get; set; }

        public bool IsValid(IServiceProvider serviceProvider)
        {
            return (Nickname == null || !string.IsNullOrWhiteSpace(Nickname) && Regex.IsMatch(Nickname, @"^[a-z0-9\.\-_]+$")) &&
                   (Bio == null || !string.IsNullOrWhiteSpace(Bio) && Bio.Length <= BioLength) &&
                   (Nickname != null || Bio != null);
        }
    }
}
