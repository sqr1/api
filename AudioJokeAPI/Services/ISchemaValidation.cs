﻿using GraphQL;
using AudioJokeAPI.Schema;

namespace AudioJokeAPI.Services
{
    public interface ISchemaValidation
    {
        public bool GetValidatableArgument<TContext, TResult>(IResolveFieldContext<TContext> context, string name,
            out TResult result) where TResult : IGraphQLValidatable;

        public bool GetTokenArgument(IResolveFieldContext<object> context, string name, out string result);
    }
}
