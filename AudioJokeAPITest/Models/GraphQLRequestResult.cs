﻿using AudioJokeAPI.ErrorHandling;
using NUnit.Framework;

namespace AudioJokeAPITest.Models
{
    public class GraphQLRequestResult<T>
    {
        public T Data { get; set; }

        public GraphQLErrorResult[] Errors { get; set; }

        public void VerifyHasExactlyOneError(string error)
        {
            Assert.That(Errors, Has.Exactly(1).Matches<GraphQLErrorResult>(x => x.Message == error));
        }
    }
}
