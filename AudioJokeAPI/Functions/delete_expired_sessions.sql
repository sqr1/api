﻿CREATE OR REPLACE FUNCTION delete_expired_sessions() RETURNS BOOLEAN AS $$
BEGIN
    DELETE FROM sessions WHERE access_token_expiry_date < now();
    RETURN true;
END;
$$ LANGUAGE plpgsql;
