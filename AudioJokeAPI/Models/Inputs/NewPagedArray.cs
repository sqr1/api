﻿using System;
using AudioJokeAPI.Schema;

namespace AudioJokeAPI.Models.Inputs
{
    public class NewPagedArray : IGraphQLValidatable
    {
        public int Page { get; set; }

        public int PerPage { get; set; }

        public bool IsValid(IServiceProvider serviceProvider)
        {
            return Page >= 0 && PerPage > 0;
        }
    }
}
