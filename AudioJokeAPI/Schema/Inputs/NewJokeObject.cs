﻿using GraphQL.Types;
using AudioJokeAPI.Models.Inputs;

namespace AudioJokeAPI.Schema.Inputs
{
    public sealed class NewJokeObject : InputObjectGraphType<NewJoke>
    {
        public NewJokeObject()
        {
            Name = "NewJoke";
            Field<StringGraphType>("text", resolve: context => context.Source.Text);
        }
    }
}
