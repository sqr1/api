﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AudioJokeAPI.Models
{
    [Table("joke")]
    public partial class Joke : BaseModel<Joke>
    {
        public Joke()
        {
            Reactions = new HashSet<Reaction>();
        }

        public Guid UserId { get; set; }
        public string Text { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Reaction> Reactions { get; set; }
    }
}
