﻿using GraphQL;
using GraphQL.Types;
using AudioJokeAPI.DataAccess;
using AudioJokeAPI.ErrorHandling;
using AudioJokeAPI.Models;
using AudioJokeAPI.Schema.Types;
using AudioJokeAPI.Services;
using System;

namespace AudioJokeAPI.Schema
{
    public class QueryObject : ObjectGraphType<object>
    {
        public QueryObject(IClientService clientUtil,
                           IDataLoaderService dataLoaderService,
                           IRepository repository,
                           ISchemaValidation schemaValidation)
        {
            Name = "Query";

            Field<UserObject>(
                "user",
                arguments: new QueryArguments(
                    new QueryArgument<GuidGraphType>
                    {
                        Name = "userId"
                    }),
                resolve: context =>
                {
                    if (context.HasArgument("userId"))
                    {
                        return dataLoaderService.GetById<User>(context.GetArgument<Guid>("userId"));
                    }

                    if (clientUtil.GetUserId() != Guid.Empty)
                    {
                        return dataLoaderService.GetById<User>(clientUtil.GetUserId());
                    }

                    return null;
                }
            );

            Field<JokeObject>(
                "joke",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<GuidGraphType>>
                    {
                        Name = "jokeId"
                    }),
                resolve: context =>
                {
                    if (clientUtil.IsRegistered())
                        return dataLoaderService.GetById<Joke>(context.GetArgument<Guid>("jokeId"));
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;

                }
            );

            Field<JokeObject>(
                "nextJoke",
                resolve: context =>
                {
                    if (clientUtil.IsRegistered())
                    {
                        Joke joke = null;
                        try
                        {
                            joke = repository.TakeNextRecommendedJoke(clientUtil.GetAccessToken());
                        }
                        catch (Npgsql.PostgresException ex)
                        {

                            GraphQLErrorHandler.RemoveAccessTokenCookieIfUnathorized(ex, clientUtil);
                            context.Errors.Add(GraphQLErrorHandler.FromPostgresException(ex));
                        }
                        return joke;
                    }
                    context.Errors.Add(GraphQLErrorHandler.NewForbiddenError());
                    return null;
                }
            );
        }
    }
}
