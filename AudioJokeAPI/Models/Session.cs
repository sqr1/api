﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AudioJokeAPI.Models
{
    [Table("sessions")]
    public partial class Session : BaseModel<Session>
    {
        public Guid UserId { get; set; }
        public string Ip { get; set; }
        public string AccessToken { get; set; }
        public DateTime AccessTokenExpiryDate { get; set; }

        public virtual User User { get; set; }
    }
}
