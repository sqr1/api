﻿CREATE OR REPLACE FUNCTION get_reacted_jokes(_user_id uuid, _reaction_type reaction_type)
    RETURNS SETOF jokes
    LANGUAGE plpgsql
AS
$$
BEGIN
    RETURN QUERY (
        SELECT p FROM jokes p JOIN reactions r ON p.id = r.joke_id WHERE r.user_id = _user_id AND r.type = _reaction_type
    );
END;
$$;
