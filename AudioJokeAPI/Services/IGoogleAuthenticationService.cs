﻿using Newtonsoft.Json;

namespace AudioJokeAPI.Services
{
    public interface IGoogleAuthenticationService
    {
        public bool Verify(string token, out GoogleAuthenticationResult result);
    }

    public class GoogleAuthenticationResult
    {
        [JsonProperty("iss")]
        public string Iss { get; set; }

        [JsonProperty("sub")]
        public string Sub { get; set; }

        [JsonProperty("azp")]
        public string Azp { get; set; }

        [JsonProperty("aud")]
        public string Aud { get; set; }

        [JsonProperty("iat")]
        public string Iat { get; set; }

        [JsonProperty("exp")]
        public string Exp { get; set; }

        /// <summary>
        /// The user's email address. This may not be unique and is not suitable for use as a primary key.
        /// Provided only if your scope included the string "email".
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// True if the user's e-mail address has been verified; otherwise false.
        /// </summary>
        [JsonProperty("email_verified")]
        public bool EmailVerified { get; set; }

        /// <summary>
        /// The user's full name, in a displayable form. Might be provided when:
        /// (1) The request scope included the string "profile"; or
        /// (2) The ID token is returned from a token refresh.
        /// When name claims are present, you can use them to update your app's user records.
        /// Note that this claim is never guaranteed to be present.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Given name(s) or first name(s) of the End-User. Note that in some cultures, people can have multiple given names;
        /// all can be present, with the names being separated by space characters.
        /// </summary>
        [JsonProperty("given_name")]
        public string GivenName { get; set; }

        /// <summary>
        /// Surname(s) or last name(s) of the End-User. Note that in some cultures,
        /// people can have multiple family names or no family name;
        /// all can be present, with the names being separated by space characters.
        /// </summary>
        [JsonProperty("family_name")]
        public string FamilyName { get; set; }

        /// <summary>
        /// The URL of the user's profile picture. Might be provided when:
        /// (1) The request scope included the string "profile"; or
        /// (2) The ID token is returned from a token refresh.
        /// When picture claims are present, you can use them to update your app's user records.
        /// Note that this claim is never guaranteed to be present.
        /// </summary>
        [JsonProperty("picture")]
        public string Picture { get; set; }

        /// <summary>
        /// End-User's locale, represented as a BCP47 [RFC5646] language tag.
        /// This is typically an ISO 639-1 Alpha-2 [ISO639‑1] language code in lowercase and an
        /// ISO 3166-1 Alpha-2 [ISO3166‑1] country code in uppercase, separated by a dash.
        /// For example, en-US or fr-CA.
        /// </summary>
        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
