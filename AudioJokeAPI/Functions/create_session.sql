﻿CREATE OR REPLACE FUNCTION create_session(_email varchar, _ip varchar) RETURNS sessions AS
$$
DECLARE
    user_    Users%ROWTYPE;
    session_ Sessions%ROWTYPE;
BEGIN
    SELECT * INTO user_ FROM users WHERE email = _email LIMIT 1;
    IF user_ IS NULL THEN
        INSERT INTO users (email) VALUES (_email);
        SELECT * INTO user_ FROM users WHERE email = _email LIMIT 1;
    END IF;
    session_.id = uuid_generate_v4();
    session_.created_at = now();
    session_.user_id = user_.id;
    session_.ip = _ip;
    session_.access_token = uuid_generate_v4();
    session_.access_token_expiry_date = now() + CONSTANT_ACCESS_TOKEN_DURATION();
    INSERT INTO sessions (id, created_at, user_id, ip, access_token, access_token_expiry_date)
    VALUES (session_.id, session_.created_at, session_.user_id, session_.ip, session_.access_token, session_.access_token_expiry_date);
    RETURN session_;
END;
$$ LANGUAGE plpgsql;
