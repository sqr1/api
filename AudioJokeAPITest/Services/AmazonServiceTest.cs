using AudioJokeAPI.Services;
using AudioJokeAPITest.Helpers;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace AudioJokeAPITest.Services
{
    public class AmazonServiceTest
    {
        private IConfiguration _configuration;

        [SetUp]
        public void SetUp()
        {
            _configuration = ConfigurationManager.Build();
        }

        [Test]
        public void TestAmazonPollyServiceSuccess()
        {
            var pollyService = new PollyService(
                _configuration.GetSection("AmazonPolly")["AccessKey"],
                _configuration.GetSection("AmazonPolly")["SecretKey"], 
                Amazon.RegionEndpoint.EUCentral1);
            var jokeAudio = pollyService.TextToSpeech("Hello, world!");

            Assert.IsNotNull(jokeAudio);
            Assert.IsNotEmpty(jokeAudio);
        }
    }
}
