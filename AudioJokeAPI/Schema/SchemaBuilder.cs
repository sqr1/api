﻿using GraphQL.Types;
using AudioJokeAPI.Models;
using AudioJokeAPI.Schema.Inputs;
using AudioJokeAPI.Schema.Types;
using Microsoft.Extensions.DependencyInjection;

namespace AudioJokeAPI.Schema
{
    public static class SchemaBuilder
    {
        public static void AddGraphQLModels(this IServiceCollection services)
        {
            services.AddScoped<EditedUserObject>();
            services.AddScoped<NewJokeObject>();
            services.AddScoped<NewPagedArrayObject>();
            services.AddScoped<NewReactionObject>();
            services.AddScoped<NewUserObject>();

            services.AddScopedWithPageArray<JokeObject, Joke>();
            services.AddScopedWithPageArray<ReactionObject, Reaction>();
            services.AddScoped<ReactionTypeObject>();
            services.AddScopedWithPageArray<SessionObject, Session>();
            services.AddScopedWithPageArray<UserObject, User>();
        }

        private static IServiceCollection AddScopedWithPageArray<N, M>(this IServiceCollection services) where N : ObjectGraphType<M>
        {
            services.AddScoped<N>();
            return services.AddScoped<PagedArrayObject<N, M>>();
        }
    }
}
