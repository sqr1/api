CREATE OR REPLACE FUNCTION delete_joke(_token varchar, _joke_id uuid) RETURNS BOOLEAN AS $$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    joke_ jokes%ROWTYPE;
BEGIN
    SELECT * INTO joke_ FROM jokes WHERE id = _joke_id LIMIT 1;
    IF joke_ IS NULL THEN
        RAISE EXCEPTION 'DELETE_JOKE_001' USING HINT = 'Invalid joke id';
    END IF;

    IF session_.user_id <> joke_.user_id
    THEN
        IF (SELECT is_admin FROM is_admin(session_.user_id) LIMIT 1)
        THEN
            RAISE EXCEPTION 'DELETE_JOKE_002' USING HINT = 'Forbidden';
        END IF;
    END IF;

    DELETE FROM jokes WHERE id = joke_.id;

    RETURN true;
END;
$$ LANGUAGE plpgsql;

