﻿using System;

namespace AudioJokeAPI.Services
{
    public interface ICDNService
    {
        public bool UploadAudioToCDN(byte[] bytes, Guid id);

        public bool DeleteAudioFromCDN(Guid id);

        public string GetAudioUrl(Guid id);
    }
}
