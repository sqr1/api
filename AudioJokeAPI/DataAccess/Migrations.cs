﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;

namespace AudioJokeAPI.DataAccess
{
    public static class Migrations
    {
        /* 
         * GENERATES PROCEDURES FOR DROPPING ALL THE ROUTINES FROM THE DATABASE
         * EXCEPTION IS USED TO AVOID ERRORS WHICH ABORT THE TRANSACTION WHEN TRYING TO DELETE PACKAGE ROUTINES
         */
        private const string GetListOfRoutineDropCommands = @"
            SELECT 'DO $$ BEGIN DROP FUNCTION ' || ns.nspname || '.' || proname
                   || '(' || oidvectortypes(proargtypes) || '); EXCEPTION WHEN dependent_objects_still_exist THEN END $$;'
            FROM pg_proc INNER JOIN pg_namespace ns ON (pg_proc.pronamespace = ns.oid)
            WHERE ns.nspname = 'public'  order by proname;
        ";

        private static string GetConnectionString()
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .Build();
            return configuration.GetConnectionString("Database");
        }

        private static int GetCurrentMigrationVersion()
        {
            Console.WriteLine(GetConnectionString());
            using var connection = new NpgsqlConnection(GetConnectionString());
            connection.Open();
            using var command = connection.CreateCommand();
            command.CommandText = "SELECT version FROM migrations ORDER BY version DESC";
            int result = 0;
            try
            {
                command.Prepare();
                result = (int)command.ExecuteScalar();
            }
            catch (PostgresException)
            {
                command.CommandText = "SELECT table_name FROM information_schema.tables WHERE table_schema NOT IN ('information_schema','pg_catalog')";
                command.Prepare();
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    throw new Exception("Database is not empty but 'migrations' table is empty or does not exist");
                }
            }

            return result;
        }

        private static string GetMigrationFileName(int version, bool isUp)
        {
            var ending = isUp ? "up" : "down";
            return Path.Combine(Directory.GetCurrentDirectory(), "Migrations", $"{version}_{ending}.sql");
        }

        private static bool DoesMigrationFileExist(int version, bool isUp)
        {
            return File.Exists(GetMigrationFileName(version, isUp));
        }

        private static void ExecuteMigrationFile(int version, bool isUp)
        {
            var request = File.ReadAllText(GetMigrationFileName(version, isUp));
            using var connection = new NpgsqlConnection(GetConnectionString());
            connection.Open();
            using var command = connection.CreateCommand();
            command.CommandText = request;
            command.ExecuteNonQuery();
            if (isUp)
            {
                command.CommandText = $"INSERT INTO migrations VALUES(uuid_generate_v4(), now(), {version})";
            }
            else
            {
                if (version > 1)
                {
                    command.CommandText = $"DELETE FROM migrations WHERE version = {version}";
                }
            }
            command.ExecuteNonQuery();
        }

        public static void UpMax()
        {
            while (Up())
            {
            }
        }

        public static bool Up()
        {
            int version = GetCurrentMigrationVersion();
            if (!DoesMigrationFileExist(version + 1, true))
            {
                Console.WriteLine("No version to upgrade.");
                return false;
            }
            ExecuteMigrationFile(version + 1, true);
            Console.WriteLine($"Migrated to version {version + 1}.");
            return true;
        }

        public static bool Down()
        {
            int version = GetCurrentMigrationVersion();
            if (!DoesMigrationFileExist(version, false))
            {
                Console.WriteLine("No version to downgrade.");
                return false;
            }
            ExecuteMigrationFile(version, false);
            Console.WriteLine($"Migrated to version {version - 1}.");
            return true;
        }

        public static void DeployFunctions()
        {
            using var connection = new NpgsqlConnection(GetConnectionString());
            connection.Open();
            using var command = connection.CreateCommand();
            command.CommandText = GetListOfRoutineDropCommands;
            List<string> commands = new List<string>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    commands.Add(reader.GetString(0));
                }
            }
            foreach (var c in commands)
            {
                command.CommandText = c;
                command.ExecuteNonQuery();
            }
            foreach (var path in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "Functions")))
            {
                command.CommandText = File.ReadAllText(path);
                command.ExecuteNonQuery();
            }
            Console.WriteLine($"Functions deployed.");
        }
    }
}
