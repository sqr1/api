using System.IO;
using System.Collections.Generic;
using Amazon.Polly;
using Amazon.Polly.Model;


namespace AudioJokeAPI.Services
{
    public class PollyService
    {
        private readonly AmazonPollyClient _client;

        public PollyService(string accessKey, string secretKey, Amazon.RegionEndpoint region)
        {
            _client = new AmazonPollyClient(accessKey, secretKey, region);
        }
        
        public byte[] TextToSpeech(string jokeText) {
            var response = _client.SynthesizeSpeechAsync(new SynthesizeSpeechRequest
            {
                LexiconNames = new List<string> {},
                OutputFormat = "mp3",
                SampleRate = "22050",
                Text = jokeText,
                TextType = "text",
                VoiceId = "Joanna"
            });

            response.Wait();
            var result = response.Result;

            byte[] data;
            if (result != null)
            {
                MemoryStream stream = new MemoryStream();
                result.AudioStream.CopyTo(stream);
                data = stream.ToArray();
            } else
            {
                data = new byte[] { 0 };
            }

            return data;
        }
    }
}
