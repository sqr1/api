CREATE OR REPLACE FUNCTION logout(_token varchar) RETURNS bool
    LANGUAGE plpgsql
AS
$$
DECLARE
    session_ sessions%ROWTYPE := check_session(_token);
BEGIN
    DELETE FROM sessions WHERE id = session_.id;
    RETURN true;
END;
$$;
