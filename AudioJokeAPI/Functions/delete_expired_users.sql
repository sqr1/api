﻿CREATE OR REPLACE FUNCTION delete_expired_users() RETURNS BOOLEAN AS $$
BEGIN
    DELETE FROM users WHERE is_registered = FALSE AND created_at + CONSTANT_NOT_REGISTERED_USER_LIVE_TIME() < now();
    RETURN true;
END;
$$ LANGUAGE plpgsql;
