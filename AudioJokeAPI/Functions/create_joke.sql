CREATE OR REPLACE FUNCTION create_joke(_token varchar, _text varchar) RETURNS Jokes AS $$
DECLARE
    session_ Sessions%ROWTYPE := check_session(_token);
    joke_id_ uuid;
    joke_ Jokes%ROWTYPE;
BEGIN
    IF (SELECT id FROM users WHERE id = session_.user_id AND is_registered LIMIT 1) IS NULL
    THEN
        RAISE EXCEPTION 'CREATE_JOKE_001' USING HINT = 'Unregistered user';
    END IF;

    joke_id_ := uuid_generate_v4();
    INSERT INTO jokes(id, user_id, text) VALUES (joke_id_, session_.user_id, _text);

    SELECT * INTO joke_ FROM jokes WHERE id = joke_id_ LIMIT 1;
    RETURN joke_;
END;
$$ LANGUAGE plpgsql;
