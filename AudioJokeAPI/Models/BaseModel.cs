﻿using System;

namespace AudioJokeAPI
{
    public abstract class BaseModel<T> where T : BaseModel<T>
    {
        public Guid Id { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
