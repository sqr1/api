﻿using System.Collections.Generic;

namespace AudioJokeAPI.Models
{
    public class PagedArray<T>
    {
        public int TotalPages { get; set; }

        public int TotalAmount { get; set; }

        public List<T> Data { get; set; }
    }
}
