﻿using AudioJokeAPI.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;

namespace AudioJokeAPI.DataAccess
{
    public class Repository : IRepository
    {
        private readonly PostgresContext _context;

        public Repository(PostgresContext context)
        {
            _context = context;
        }

        private NpgsqlParameter CreateNpgsqlParameter(string key, object value)
        {
            return new NpgsqlParameter(key, value ?? DBNull.Value);
        }

        public User GetUserByAccessToken(string accessToken)
        {
            return _context.Users.FromSqlRaw("SELECT * FROM get_user_by_access_token(@accessToken)",
                CreateNpgsqlParameter("@accessToken", accessToken)).FirstOrDefault();
        }

        public PagedArray<T> GetPage<T>(int page, int perPage, System.Linq.Expressions.Expression<Func<T, bool>> predicate = null) where T : BaseModel<T>
        {
            var result = new PagedArray<T>();
            IQueryable<T> request;
            if (predicate == null)
            {
                request = _context.Set<T>().OrderByDescending(x => x.CreatedAt).Skip(page * perPage);
            }
            else
            {
                request = _context.Set<T>().Where(predicate).OrderByDescending(x => x.CreatedAt).Skip(page * perPage);
            }
            result.TotalAmount = request.Count();
            result.TotalPages = (int)Math.Ceiling((double)result.TotalAmount / perPage);
            result.Data = request.Take(perPage).ToList();
            return result;
        }

        public PagedArray<T> GetPage<T>(int page, int perPage, IQueryable<T> queryable) where T : BaseModel<T>
        {
            var result = new PagedArray<T>();
            var request = queryable.OrderByDescending(x => x.CreatedAt).Skip(page * perPage);
            result.TotalAmount = request.Count();
            result.TotalPages = (int)Math.Ceiling((double)result.TotalAmount / perPage);
            result.Data = request.Take(perPage).ToList();
            return result;
        }

        public Task<IDictionary<Guid, T>> GetModelsByIdAsync<T>(IEnumerable<Guid> ids, CancellationToken cancellationToken) where T : BaseModel<T>
        {
            return _context.Set<T>().Where(x => ids.Contains(x.Id)).AsNoTracking().ToDictionaryAsync(x => x.Id, cancellationToken).ContinueWith<IDictionary<Guid, T>>(task => task.Result);
        }

        public Joke CreateJoke(string accessToken, string text)
        {
            return _context.Jokes.FromSqlRaw("SELECT * FROM create_joke(@accessToken, @text)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@text", text)).FirstOrDefault();
        }

        public Reaction CreateReaction(string accessToken, Guid jokeId, ReactionType type)
        {
            return _context.Reactions.FromSqlRaw("SELECT * FROM create_reaction(@accessToken, @jokeId, @type)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@jokeId", jokeId),
                CreateNpgsqlParameter("@type", type)).FirstOrDefault();
        }

        public void DeleteJoke(Guid id)
        {
            _context.Jokes.Remove(new Joke { Id = id });
            _context.SaveChanges();
        }

        public void DeleteJoke(string accessToken, Guid id)
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM delete_joke(@accessToken, @id)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@id", id));
        }

        public void DeleteReaction(string accessToken, Guid id)
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM delete_reaction(@accessToken, @id)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@id", id));
        }

        public void DeleteUser(string accessToken, Guid id)
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM delete_user(@accessToken, @id)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@id", id));
        }

        public void EditUser(string accessToken, Guid id, string nickname, string bio)
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM edit_user(@accessToken, @id, @nickname, @bio)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@id", id),
                CreateNpgsqlParameter("@nickname", nickname),
                CreateNpgsqlParameter("@bio", bio));
        }

        public IQueryable<Joke> GetReactedJokes(Guid userId, ReactionType reactionType)
        {
            return _context.Set<Joke>().FromSqlRaw("SELECT * FROM get_reacted_jokes(@userId, @reactionType)",
                CreateNpgsqlParameter("@userId", userId),
                CreateNpgsqlParameter("@reactionType", reactionType)).OrderBy(p => p.CreatedAt);
        }

        public IQueryable<Joke> GetPublishedJokes(Guid userId)
        {
            return _context.Jokes.Where(p => p.UserId == userId).OrderBy(p => p.CreatedAt);
        }

        public void Logout(string accessToken)
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM logout(@accessToken)",
                CreateNpgsqlParameter("@accessToken", accessToken));
        }

        public Session CreateSession(string email, string ip)
        {
            return _context.Sessions.FromSqlRaw("SELECT * FROM create_session(@email, @ip)",
                CreateNpgsqlParameter("@email", email),
                CreateNpgsqlParameter("@ip", ip)).FirstOrDefault();
        }

        public Session Refresh(string accessToken)
        {
            return _context.Sessions.FromSqlRaw("SELECT * FROM refresh(@accessToken)",
                CreateNpgsqlParameter("@accessToken", accessToken)).FirstOrDefault();
        }

        public User RegisterUser(string accessToken, string nickname)
        {
            return _context.Users.FromSqlRaw($"SELECT * FROM register_user(@accessToken, @nickname)",
                CreateNpgsqlParameter("@accessToken", accessToken),
                CreateNpgsqlParameter("@nickname", nickname)).FirstOrDefault();
        }

        public void DeleteExpiredSesssions()
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM delete_expired_sessions()");
        }

        public void DeleteExpiredUsers()
        {
            _context.Database.ExecuteSqlRaw("SELECT * FROM delete_expired_users()");
        }

        public Joke TakeNextRecommendedJoke(string accessToken)
        {
            var rand = new Random();
            var skip = (int)(rand.NextDouble() * _context.Jokes.Count());
            return _context.Jokes.OrderBy(o => o.Id).Skip(skip).Take(1).FirstOrDefault();
        }
    }
}
